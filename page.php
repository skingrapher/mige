<?php
global $more;
$more = -1;

get_header();

while (have_posts()) : the_post(); 
    the_title("<h2>", "</h2>");
?>
        <article>
        <?php the_content(); ?>
        </article>
<?php
endwhile; 
?>

  </main>

<?php get_sidebar(); ?>

</div>

<?php
get_footer();
