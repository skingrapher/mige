<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package mige
 */

get_header(); ?>

<div class="row container section">
    <main class="col s12 m9">

    <?php
    if ( have_posts() ) :
    ?>
    <h2>
        <?php
        /* translators: %s: search query. */
        printf( _e( "Search results for: ", "mige" )."%s", "<strong>" . get_search_query() . "</strong>" );
        ?>
    </h2>

    <?php
        while ( have_posts() ) : the_post();
            get_template_part( 'parts/card' );
        endwhile;

    else :
    ?>
    <p class="red-text darken-4"><?php _e("Nothing found", "mige"); ?></p>
    <?php
    endif;
    ?>

    </main>
<?php get_sidebar(); ?>
</div>

<?php
get_footer();
