<?php
global $more;
$more = -1;

get_header();

while (have_posts()) : the_post(); 
    get_template_part( 'parts/contact' );
endwhile; 
?>

  </main>

<?php get_sidebar(); ?>

</div>

<?php
get_footer();
