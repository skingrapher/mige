<?php
/*
 * PRODUCERS 
 */

// types de commerce pour les marchés
$serv_cases = get_terms([
    "taxonomy"      => "classement",
    "hide_empty"    => true
]);

if(!empty($serv_cases)) : ?>
    <h2><?php _e("Services", "mige"); ?></h2>
<?php
    foreach($serv_cases as $case) :
        $term = $case->name;
        $term_link = get_term_link($case->slug, "classement");
        $term_markup = "<span class=\"chip\"><a href=\"%s\">%s</a></span>";
        printf($term_markup, $term_link, $term);
    endforeach;
endif;

