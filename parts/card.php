    <div class="card hoverable">

    <?php
            // if has thumbnail
            if (has_post_thumbnail()) :
            
                // available formats: thumbnail, medium, medium_large, large, full
                // @link https://www.smashingmagazine.com/2016/09/responsive-images-in-wordpress-with-art-direction/
                $thumbnail_format = "thumbnail"; // possible values: thumbn
                $thumbnail_id = get_post_thumbnail_id($post_id);
                $thumbnail_src = wp_get_attachment_image_src($thumbnail_id, "thumbnail");
                $thumbnail_srcset = wp_get_attachment_image_srcset($thumbnail_id, "thumbnail");
                $thumbnail_sizes = wp_get_attachment_image_sizes($thumbnail_id, "thumbnail");
                $thumbnail_alt = get_post_meta($thumbnail_id, "_wp_attachment_image_alt", true);

    ?>
        <figure class="card-image">

          <img class="lazyload blur responsive-img"
            srcset="<?php print  esc_attr($thumbnail_srcset); ?>"
            data-srcset="<?php print  esc_attr($thumbnail_srcset); ?>"
            src="<?php print  esc_attr($thumbnail_src); ?>"
            alt="<?php print  esc_attr($thumbnail_alt); ?>"
            sizes="<?php print esc_attr($thumbnail_sizes); ?>"/>

          <figcaption class="card-title">
              <h3><?php the_title(); ?></h3>
          </figcaption>

        </figure>

        <?php if ( has_excerpt() ) : ?>
        <div class="card-content truncate">
            <?php the_excerpt; ?>
        </div>
        <?php endif;

            // if has not thumbnail
            else :
        ?>

        <div class="card-content">
            <h3 class="card-title"><?php the_title(); ?></h3>

            <?php if ( has_excerpt() ) : ?>
            <p><?php the_excerpt; ?></p>
            <?php endif; ?>

        </div>

        <?php endif; // end of conditional statement for thumbnail ?>

        <div class="card-action">
            <a href="<?php the_permalink() ?>"><?php _e("Read more", "mige"); ?></a>
        </div>

    </div>

