<!-- fil d'ariane -->
<div class="valign-wrapper">
    <span class="material-icons small" aria-hidden="true">home</span>
    <span class="screen-reader-text"><?php _e("Home", "mige"); ?></span>
    
<?php
// FRONT PAGE WITHOUT LINK
if(is_front_page()) :
    _e("Home","mige");
// FRONT PAGE WITH LINK
else :  ?>
    <a href="<?php print esc_url( home_url( '/' ) ); ?>"><?php _e("Home","mige"); ?></a>
<?php
endif;
/*
 * HOME PAGE
 */
if( is_home() && get_option("page_for_posts") ) :
?>
    <span class="material-icons tiny" aria-hidden="true">navigate_next</span>
    <span><?php print get_the_title( get_option("page_for_posts") ); ?></span>
<?php
/*
 * NOT HOME PAGE
 * NOT FRONT PAGE
 * SINGLE POST
 * CUSTOM POST OR NOT
 */
elseif( ( is_page() && !is_front_page() ) || is_single() ) :
    $mige_post_type = get_post_type();

    if($mige_post_type != false && $mige_post_type == "post") : ?>

    <span class="material-icons tiny" aria-hidden="true">navigate_next</span>
    <span><?php print get_the_title( get_option("page_for_posts") ); ?></span>
<?php
    elseif($mige_post_type != false && $mige_post_type != "post") :
        $mige_post_obj = get_post_type_object($mige_post_type);
?>
    <span class="material-icons tiny" aria-hidden="true">navigate_next</span>
    <span><?php print $mige_post_obj->label; ?></span>
<?php
    endif;
?>

    <span class="material-icons tiny" aria-hidden="true">navigate_next</span>
    <span><?php the_title("<span>", "</span>"); ?></span>

<?php
/*
 * ARCHIVE
 */
elseif( is_archive() ) :
?>
    <span class="material-icons tiny" aria-hidden="true">navigate_next</span>
    <span><?php print get_the_archive_title(); ?></span>
<?php
/*
 * SEARCH
 */
elseif( is_search() ) :
?>
    <span class="material-icons tiny" aria-hidden="true">navigate_next</span>
    <span><?php print get_search_query(); ?></span>
<?php endif; ?>
</div>





