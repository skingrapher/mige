<?php
/*
 * PAGINATION BAR
 */
    // how many pages exist

/*
 * PAGINATION
 * @link http://materializecss.com/pagination.html
 */

?>

<ul class="pagination center-align">

<?php
//if(is_home()) :

    global $wp_query;

    $total_pages = $wp_query->max_num_pages;
    // if there is more than 1 page to paginate
    if($total_pages > 1) :

        $current_page_nb = max(1, get_query_var("paged"));

        // CHECK IF LINK TO PREVIOUS PAGE
        if(get_previous_posts_link() == void) :
            $prev_li_class = "disabled";
            $prev_link = null;
        else :
            $prev_li_class = "waves-effect";
            $prev_link = get_previous_posts_link("<span class=\"material-icons\" aria-hidden=\"true\">chevron_left</span>");
        endif;

        // CHECK IF LINK TO NEXT PAGE
        if(get_next_posts_link() == void) :
            $next_li_class = "disabled";
            $next_link = null;
        else :
            $next_li_class = "waves-effect";
            $next_link = get_next_posts_link("<span class=\"material-icons\" aria-hidden=\"true\">chevron_right</span>");
        endif;

?>

    <li class="<?php print $prev_li_class; ?>"><?php if($prev_link != null) : print $prev_link; endif; ?></li>

<?php
            $mige_pagenum_link = get_pagenum_link();

            $mige_paginate_args = [
                "base"      => $mige_pagenum_link."%_%",
                "format"    => "page/%#%",
                "total"     => $total_pages,
                "current"   => $current_page_nb,
                "show_all"  => false,
                "end_size"  => 2,
                "mid_size"  => 3,
                "prev_next" => false,
                "type"      => "array"
            ];

            $mige_paginate_links = paginate_links($mige_paginate_args);

            foreach($mige_paginate_links as $link) :

                $link_key = key($mige_paginate_links);
                if ( ($link_key + 1) != intval($current_page_nb) ) : ?>

    <li class="waves-effect"><?php print $link; ?></li>

<?php
                else : ?>

    <li class="active"><a href="#"><?php print $link; ?></a></li>

<?php
                endif;
                next($mige_paginate_links);
            endforeach;
?>

    <li class="<?php print $next_li_class; ?>"><?php if($next_link != null) : print $next_link; endif; ?></li>

<?php
    endif;
//endif;
?>
    
</ul>
