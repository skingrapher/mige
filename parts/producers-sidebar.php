<?php
/*
 * PRODUCERS 
 */

// types de commerce pour les marchés
$products = get_terms([
    "taxonomy"      => "produit",
    "hide_empty"    => true
]);

if(!empty($products)) : ?>
    <h2><?php _e("Producers", "mige"); ?></h2>
<?php
    foreach($products as $product) :
        $term = $product->name;
        $term_link = get_term_link($product->slug, "produit");
        $term_markup = "<span class=\"chip\"><a href=\"%s\">%s</a></span>";
        printf($term_markup, $term_link, $term);
    endforeach;
endif;

