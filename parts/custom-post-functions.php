<?php

/*
 * CUSTOM POST TYPES
 * must be called AFTER after_setup_theme hook AND BEFORE admin_menu hook
 * @link https://codex.wordpress.org/Post_Types
 */
if ( ! function_exists( 'mige_custom_post_types' ) ) :
    function mige_custom_post_types(){

        // SERVICES 
        $serv_labels = [
            "name"          => __("Services", "mige"),
            "singular_name" => __("Service", "mige"),
            "add_new"       => "Ajouter", 
            "add_new_item"  => __("Add new service", "mige"),
            "edit_item"     => __("Edit service", "mige"),
            "new_item"      => __("New service", "mige"),
            "view_item"     => __("View service", "mige"),
            "all_items"     => __("All services", "mige"),
            "not_found"     => __("No service found.", "mige"),
            "not_found_in_trash"    => __("no service found in the trash.", "mige"),
            "search_items"          => __("Search services", "mige"),
            "archives"      => __("Archived services", "mige")
        ];

        $serv_supports = [
            "title",
            "excerpt",
            "editor",
            "thumbnail",
            "custom-fields",
        ];

        $serv_args = [
            "labels"                => $serv_labels,
            "public"                => true,
            "description"           => "services commerciaux",
            "exclude_from_search"   => false,
            "show_in_nav_menu"      => false,
            "menu_icon"             => "dashicons-awards",
            "capability_type"       => "post",
            "menu_position"         => 20,
            "supports"              => $serv_supports
        ];

        register_post_type("service", $serv_args);

        // TAXO CASE 
        $taxo_serv_labels = [
            "name"          => __("Cases", "mige"),
            "singular_name" => __("Case", "mige"),
            //"add_new"       => _x("Add new", "questionnaire", "mige"),
            "add_new"       => "Ajouter", 
            "add_new_item"  => __("Add new case", "mige"),
            "edit_item"     => __("Edit case", "mige"),
            "new_item"      => __("New case", "mige"),
            "view_item"     => __("View case", "mige"),
            "all_items"     => __("All cases", "mige"),
            "not_found"     => __("No case found.", "mige"),
            "not_found_in_trash"    => __("no case found in the trash.", "mige"),
            "search_items"          => __("Search cases", "mige"),
            "archives"      => __("Archived cases", "mige")
        ];
        
        $taxo_serv_args = [
            "labels"                => $taxo_serv_labels,
            "public"                => true,
            "description"           => "classement des services",
            "show_in_nav_menu"      => false,
            "hierarchical"          => true,
            "show_tag_cloud"        => false // not displayed in widgets
        ];

        register_taxonomy("classement", "service", $taxo_serv_args);
        register_taxonomy_for_object_type("classement", "service");

        // PRODUCERS
        $prod_labels = [
            "name"          => __("Producers", "mige"),
            "singular_name" => __("Producer", "mige"),
            "add_new"       => "Ajouter", 
            "add_new_item"  => __("Add a new producer", "mige"),
            "edit_item"     => __("Edit producer", "mige"),
            "new_item"      => __("New producer", "mige"),
            "view_item"     => __("View producer", "mige"),
            "all_items"     => __("All producers", "mige"),
            "not_found"     => __("No producer found.", "mige"),
            "not_found_in_trash"    => __("no producer found in the trash.", "mige"),
            "search_items"          => __("Search producers", "mige"),
            "archives"      => __("Archived producers", "mige")
        ];

        $prod_supports = [
            "title",
            "excerpt",
            "editor",
            "thumbnail",
            "custom-fields",
        ];

        $prod_args = [
            "labels"                => $prod_labels,
            "public"                => true,
            "description"           => "producteurs locaux",
            "exclude_from_search"   => false,
            "show_in_nav_menu"      => false,
            "menu_icon"             => "dashicons-carrot",
            "capability_type"       => "post",
            "menu_position"         => 20,
            "rewrite"               => ["slug"  => "producteur"],
            "supports"              => $prod_supports
        ];

        register_post_type("producteur", $prod_args);

        // TAXO PRODUCT 
        $taxo_prod_labels = [
            "name"          => __("Products", "mige"),
            "singular_name" => __("Product", "mige"),
            //"add_new"       => _x("Add new", "questionnaire", "mige"),
            "add_new"       => "Ajouter", 
            "add_new_item"  => __("Add new product", "mige"),
            "edit_item"     => __("Edit product", "mige"),
            "new_item"      => __("New product", "mige"),
            "view_item"     => __("View product", "mige"),
            "all_items"     => __("All products", "mige"),
            "not_found"     => __("No product found.", "mige"),
            "not_found_in_trash"    => __("no product found in the trash.", "mige"),
            "search_items"          => __("Search products", "mige"),
            "archives"      => __("Archived products", "mige")
        ];
        
        $taxo_prod_args = [
            "labels"                => $taxo_prod_labels,
            "public"                => true,
            "description"           => "classement des produits",
            "show_in_nav_menu"      => false,
            "hierarchical"          => true,
            "show_tag_cloud"        => false // not displayed in widgets
        ];

        register_taxonomy("produit", "producteur", $taxo_prod_args);
        register_taxonomy_for_object_type("produit", "producteur");

        // QUESTIONNAIRE
        $q_labels = [
            "name"          => __("Questionnaires", "mige"),
            "singular_name" => __("Questionnaire", "mige"),
            //"add_new"       => _x("Add new", "questionnaire", "mige"),
            "add_new"       => "Ajouter", 
            "add_new_item"  => __("Add new questionnaire", "mige"),
            "edit_item"     => __("Edit questionnaire", "mige"),
            "new_item"      => __("New questionnaire", "mige"),
            "view_item"     => __("View questionnaire", "mige"),
            "all_items"     => __("All questionnaires", "mige"),
            "not_found"     => __("No questionnaire found.", "mige"),
            "not_found_in_trash"    => __("no questionnaire found in the trash.", "mige"),
            "search_items"          => __("Search questionnaires", "mige"),
            "archives"      => __("Archived questionnaires", "mige")
        ];

        $q_supports = [
            "title",
            "thumbnail"
        ];

        $q_args = [
            "labels"                => $q_labels,
            "public"                => true,
            "description"           => "publish questionnaire",
            "exclude_from_search"   => true,
            "show_in_nav_menu"      => false,
            "menu_icon"             => "dashicons-forms",
            "capability_type"       => "post",
            "menu_position"         => 20,
            "supports"              => $q_supports
        ];

        register_post_type("questionnaire", $q_args);

        // MARKET
        $m_labels = [
            "name"          => __("Markets", "mige"),
            "singular_name" => __("Market", "mige"),
            "add_new"       => "Ajouter", 
            "add_new_item"  => __("Add new market", "mige"),
            "edit_item"     => __("Edit market", "mige"),
            "new_item"      => __("New market", "mige"),
            "view_item"     => __("View market", "mige"),
            "all_items"     => __("All markets", "mige"),
            "not_found"     => __("No market found.", "mige"),
            "not_found_in_trash"    => __("no market found in the trash.", "mige"),
            "search_items"          => __("Search markets", "mige"),
            "archives"      => __("Archived markets", "mige")
        ];

        $m_supports = [
            "title",
            "excerpt",
            "editor",
            "thumbnail",
            "custom-fields",
        ];

        $m_args = [
            "labels"                => $m_labels,
            "public"                => true,
            "description"           => "Marchés partenaires de la Mige",
            "exclude_from_search"   => false,
            "show_in_nav_menu"      => false,
            "menu_icon"             => "dashicons-store",
            "capability_type"       => "post",
            "menu_position"         => 20,
            "has_archive"           => true,
            "supports"              => $m_supports
        ];

        register_post_type("market", $m_args);

        // TAXO COMMERCE
        $taxo_commerce_labels = [
            "name"          => __("Trades", "mige"),
            "singular_name" => __("Trade", "mige"),
            //"add_new"       => _x("Add new", "questionnaire", "mige"),
            "add_new"       => "Ajouter", 
            "add_new_item"  => __("Add new trade", "mige"),
            "edit_item"     => __("Edit trade", "mige"),
            "new_item"      => __("New trade", "mige"),
            "view_item"     => __("View trade", "mige"),
            "all_items"     => __("All trades", "mige"),
            "not_found"     => __("No trade found.", "mige"),
            "not_found_in_trash"    => __("no trade found in the trash.", "mige"),
            "search_items"          => __("Search trades", "mige"),
            "archives"      => __("Archived trades", "mige")
        ];
        
        $taxo_commerce_args = [
            "labels"                => $taxo_commerce_labels,
            "public"                => true,
            "description"           => "classement des marchés par leur type de commerce",
            "show_in_nav_menu"      => false,
            "hierarchical"          => true,
            "show_tag_cloud"        => false // not displayed in widgets
        ];

        register_taxonomy("commerce", "market", $taxo_commerce_args);
        register_taxonomy_for_object_type("commerce", "market");

        // TAXO LIEU
        $taxo_lieu_labels = [
            "name"          => __("Places", "mige"),
            "singular_name" => __("Place", "mige"),
            "add_new"       => "Ajouter", 
            "add_new_item"  => __("Add new place", "mige"),
            "edit_item"     => __("Edit place", "mige"),
            "new_item"      => __("New place", "mige"),
            "view_item"     => __("View place", "mige"),
            "all_items"     => __("All places", "mige"),
            "not_found"     => __("No place found.", "mige"),
            "not_found_in_trash"    => __("no place found in the trash.", "mige"),
            "search_items"          => __("Search places", "mige"),
            "archives"      => __("Archived places", "mige")
        ];
        
        $taxo_lieu_args = [
            "labels"                => $taxo_lieu_labels,
            "public"                => true,
            "description"           => "classement des places de marché en Creuse",
            "show_in_nav_menu"      => false,
            "hierarchical"          => true,
            "show_tag_cloud"        => false // not displayed in widgets
        ];

        register_taxonomy("lieu", "market", $taxo_lieu_args);
        register_taxonomy_for_object_type("lieu", "market");
    }
endif;

/*
 * META BOXES
 * FOR QUESTIONNAIRES
 */
if ( ! function_exists( 'questionnaire_box' ) ) :
    function questionnaire_box(){

        function champs(){
            global $post;
            $meta = get_post_meta($post->ID, "question", true); 

            // all fields
            // @link https://www.taniarascia.com/wordpress-part-three-custom-fields-and-metaboxes/
            // type de questionnaire: à choix unique (boutons radio) ou à choix multiple(checkboxes)
             ?>

            <select name="question[choix]">
                <option><?php _e("Which questionnaire type?", "mige"); ?></option>
                <option id="choix-unique" value="unique" <?php print selected($meta["choix"], "unique", false); ?>><?php _e("unique choice", "mige"); ?></option>
                <option id="choix-multiple" value="multiple" <?php print selected($meta["choix"], "multiple", false); ?>><?php _e("multiple choice", "mige"); ?></option>
            </select>

            <?php

            if(wp_script_is("questionnaire", "enqueued")) : 
                require get_template_directory() . "/parts/questionnaire.php";
            endif;
        }

        add_meta_box(
            "question",
            __("Questionnaire type", "mige"),
            "champs",
            "questionnaire",
            "normal",
            "high"
        );

        function resultats(){
            global $wpdb;
            global $post;
            $meta = get_post_meta($post->ID, "reponses", true);

            // ON EDIT PAGE
            // NOT ON NEW POST PAGE
            if( isset( $_GET["post"]) ) :

                $questionnaire_result_cache = wp_cache_get($post->ID, "questionnaire_result_cache");

                if($questionnaire_result_cache === false) :
                    $query = "SELECT choix, rep, rep_alt FROM ".$wpdb->prefix."questionnaire_results WHERE post_id = ".$post->ID;
                    $result = $wpdb->get_results($query, ARRAY_A);

                    wp_cache_add($post-ID, $result, "questionnaire_result_cache");
                else :
                    $result = $questionnaire_result_cache;
                endif;

                ////---------- QUESTIONNAIRE ON EDIT PAGE ONLY ----------////
                $reps = array_column($result, "rep");

                // IF TABLE IS EMPTY
                if( empty( count($reps) ) ) : ?>
                    <p><?php _e("No one has answered this questionnaire.", "mige"); ?></p>
                <?php
                // IF ANY ANSWER TO QUESTIONNAIRE
                // ADD CANVAS FOR VISUAL REPRESENTATION
                // OF RESULTS
                else : ?>

            <div id="results__view"></div>

                <?php

                    // RESULTS FOR UNIQUE CHOICE QUESTIONNAIRE
                    $choice = array_column($result, "choix");
                    $choice = $choice[0];

                    $votes = []; 

                    if($choice == "unique") :
                        $reps = array_filter($reps, function($val){ return !is_null($val); });

                        $counts = array_count_values($reps);

                        foreach($counts as $rep => $count) :
                            $vote = sprintf('{ "rep": "%s", "votes": %d}', $rep, $count);
                            array_push($votes, $vote);
                        endforeach;

                    // RESULTS FOR MULTIPLE CHOICE QUESTIONNAIRE
                    elseif($choice == "multiple") :
                        foreach($reps as $rep) :
                            // explode all responses in string
                            $rep = explode(",", $rep);

                            // put in each item of array in votes array
                            array_map(
                                function ($r) {
                                    array_push($votes, $r);
                                },
                                $rep
                            );
                        endforeach;

                        // count each response
                        // returns array
                        //  key: response
                        //  value: number of answers for this response
                        $votes = array_filter($votes, function($val){ return !is_null($val); });
                        $counts = array_count_values($votes);
                        // empty array
                        $votes = []; 

                        // explode like above
                        foreach($counts as $rep => $count) :
                            $vote = sprintf('{ "rep": "%s", "votes": %d}', $rep, $count);
                            array_push($votes, $vote);
                        endforeach;

                    endif;

                    // PRINT RESULTS IN JS VAR
                    printf("<script>let questionnaire_results = [%s]</script>", join(",", $votes));

                    $questionnaire_alt_rep_cache = wp_cache_get($post->ID, "questionnaire_alt_rep_cache");

                    if($questionnaire_alt_rep_cache === false) :
                        $alt_query = "SELECT rep_alt FROM ".$wpdb->prefix."questionnaire_results WHERE post_id = ".$post->ID." AND rep_alt IS NOT NULL";
                        $alt_result = $wpdb->get_results($alt_query, ARRAY_A);

                        wp_cache_add($post-ID, $alt_result, "questionnaire_alt_rep_cache");
                    else :
                        $alt_result = $questionnaire_alt_rep_cache;
                    endif;

                    // OTHER ANSWERS (FREE EXPRESSION)
                    $rep_alt = array_column($alt_result, "rep_alt");
                    $rep_alt = array_unique($rep_alt, SORT_LOCALE_STRING);
                    $rep_alt_counts = array_count_values($rep_alt);

                    if(empty( $rep_alt_counts )) :
                        print "<p>".__("Other answers: ", "mige")."<strong>0</strong></p>";
                    else :
                        $rep_alt_total = count($rep_alt, COUNT_RECURSIVE);
                        printf("<p>".__("Other answers: ", "mige")."<strong>%d</strong></p>", $rep_alt_total);

                        // BEGIN LIST
                        print "<ol>";
                        
                        $rep_alt_counts = array_flip($rep_alt_counts);

                        foreach($rep_alt_counts as $rep) :
                            printf("<li>%s</li>", stripcslashes($rep));
                        endforeach;

                        // END LIST
                        print "</ol>";
                    endif;
                    ////---------- END OF QUESTIONNAIRE ON EDIT PAGE ----------////

                endif;
            endif;
        }

        add_meta_box(
            "reponses",
            __("Results", "mige"),
            "resultats",
            "questionnaire",
            "normal",
            "low"
        );
    }
endif;

// save questionnaire data
if ( ! function_exists( 'save_question_meta' ) ) :
    function save_question_meta($id){
        // verify nonce
        if($_POST["post_type"] == "questionnaire"){
            if(!wp_verify_nonce($_POST["_qn"], "validation")){
                return $id;
            }
            // check autosave
            elseif(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE){
                return $id;
            }
            elseif(!current_user_can("edit_post", $id)){
                return $id;
            }
            else{
                $old_meta = get_post_meta($post->ID, "question", true);
                $new_meta = $_POST["question"];

                if(isset($new_meta) == true && $new_meta  != $old_meta){
                    update_post_meta($id, "question", $new_meta);
                }
            }
        }
        else{
            return $id;
        }
    }
endif;

// delete data from sql table
if ( ! function_exists( 'del_question_meta' ) ) :
    function del_question_meta(){
        global $post;
        global $wpdb;
        $id = $post->ID;
        $table = $wpdb->prefix."questionnaire_results";
        $wpdb->query($wpdb->prepare("DELETE from %s WHERE post_id = %s", [$table, $id]));
    }
endif;

