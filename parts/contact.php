<?php
/*
 * INPUT VALUES WITH POST VARIABLES
 * in case of a wrongly submitted form,
 * all data is retained so the user
 * doesn’t have to type out everything
 * from scratch
 */

?>
        <article class="container">

            <h2><?php _e("Contact", "mige"); ?></h2>
            <?php the_content(); ?>

            <form id="contact__form" class="row" action="<?php the_permalink(); ?>" method="post">

                <?php $mige_authorize = get_post_meta(get_the_ID(), "autorisation_RGPD", true);

                if (!empty($mige_authorize)) : ?>

                <p class="red-text darken-4 container" id="contact__warning"><?php print $mige_authorize; ?></p>

                <?php endif; ?>

                <!-- SWITCH -->
                <div class="switch right-align">
                    <label><? _e("no", "mige"); ?><input type="checkbox" id="contact__switch"><span class="lever"></span><?php _e("yes", "mige"); ?></label>
                </div>

                <!-- USERNAME -->
                <div class="input-field col s12 m6">
                    <input inputmode="text" autocomplete="name" class="validate" placeholder="" name="nom" id="contact__name" required type="text" maxlength="100" value="<?php if( isset($_POST["nom"]) ) : print esc_attr($_POST["nom"]); endif; ?>"/>
                    <label for="contact__name"><span class="material-icons small" aria-hidden="true">person</span><?php _e("Name", "mige"); ?></label>
                </div>

                <!-- EMAIL -->
                <div class="input-field col s12 m6">
                    <input inputmode="email" autocomplete="email" class="validate" placeholder="" name="mel" id="contact__email" required type="email" pattern="^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$" maxlength="120" value="<?php if( isset($_POST["mel"]) ) : print esc_attr($_POST["mel"]); endif; ?>"/>
                    <label for="contact__email"><span class="material-icons small" aria-hidden="true">mail_outline</span><?php _e("Email", "mige"); ?></label>
                </div>

                <!-- CAPTCHA -->
                <div class="input-field col s12">
                    <input inputmode="numeric" class="validate" name="captcha" id="contact__captcha" required type="text" pattern="^\d{4}$" value="" maxlength="4"/>
                    <label for="contact__captcha"><span class="material-icons small" aria-hidden="true">label_outline</span><?php _e("Enter captcha code: ", "mige"); ?><span id="captchacode"><?php $captcha = randomCaptcha(); print $captcha; ?></span></label>
                    <button type="button" id="renewcaptcha" class="right waves-effect waves-light btn-floating">
                        <span class="material-icons small" aria-hidden="true">autorenew</span>
                        <span class="screen-reader-text"><?php _e("Renew", "mige"); ?></span>
                    </button>
                </div>

                <!-- MESSAGE -->
                <div class="input-field col s12">
                    <textarea class="materialize-textarea" name="msg" required aria-required="true" id="contact__msg" value="<?php if( isset($_POST["msg"]) ) : print esc_attr($_POST["msg"]); endif; ?>"></textarea>
                    <label for="contact__msg"><span class="material-icons small" aria-hidden="true">message</span><?php _e("Message", "mige"); ?></label>
                </div>

            </form>


            <!--
            SUBMIT
            ------
            FF BUG
            @link https://developer.mozilla.org/fr/docs/Web/HTML/Element/Button
            @link https://bugzilla.mozilla.org/show_bug.cgi?id=654072 
            -->
            <button id="contact__submit" form="contact__form" class="btn waves-effect waves-light disabled" disabled autocomplete="off" type="submit">
                <?php _e("Send", "mige"); ?>
                <span class="material-icons right" aria-hidden="true">send</span>
            </button>

            <p><small>La demande de code sert de filtre antispam.</small></p>
            </script>
<?php
// check if POST data
if( count($_POST) != 0 ){
    // send email
    if( isset($_POST["nom"]) && isset($_POST["mel"]) && isset($_POST["msg"]) ){
        
        poster($_POST["nom"], $_POST["mel"], $_POST["msg"]);

    }
}
?>
        </article>

        <script>
// CREDIT
// adapted from Mario Vidor's pen
// @author Mario Vidor
// @link https://codepen.io/mel/pen/kHCvr
function captchaCode() {
    let nb0, nb1, nb2, nb3, captchaCode;     
    nb0 = (Math.ceil(Math.random() * 10)-1).toString();
    nb1 = (Math.ceil(Math.random() * 10)-1).toString();
    nb2 = (Math.ceil(Math.random() * 10)-1).toString();
    nb3 = (Math.ceil(Math.random() * 10)-1).toString();

    captchaCode = nb0+nb1+nb2+nb3;

    // REPLACE CURRENT CAPTCHA CODE
    let captchaSpan = document.getElementById('captchacode');
    let newSpan = document.createElement("span");
    let newSpanCode = document.createTextNode("<?php __('Enter captcha code: ', 'mige'); ?>" + captchaCode);

    newSpan.setAttribute("id", "captchacode");
    newSpan.appendChild(newSpanCode);

    captchaSpan.parentNode.replaceChild(newSpan, captchaSpan);

    // REMOVE INVALID CLASS FROM INPUT
    let captchaInput = document.getElementById("contact__captcha");

    if(captchaInput.classList.contains("invalid")){
        captchaInput.classList.remove("invalid")
    }
}

function validateCaptcha(){
    let captchaCode = document.getElementById("captchacode").innerHTML;
    let enteredCode = document.getElementById("contact__captcha").value;
    let submitButton = document.getElementById("contact__submit");

    // validated code
    if(enteredCode == captchaCode){
        submitButton.removeAttribute("disabled");
        submitButton.classList.remove("disabled");
    }
    else{
        submitButton.classList.add("invalid");   
    }
}

// EVENT LISTENER ON CAPTCHA RENEW BUTTON
function renewCaptcha(el){
    el.addEventListener = ("click", () => {
        captchaCode();
    });
}

document.getElementById("renewcaptcha").addEventListener("click", () => {
    captchaCode()
});

document.getElementById("contact__captcha").addEventListener("blur", () => {
    validateCaptcha();
});

        </script>
