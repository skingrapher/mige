<script>
<?php
if(is_page("contact")) : ?>
let contactSwitch = document.getElementById("contact__switch");

// toggle disabled state of inputs in contact form
function toggle_inputs_disabled(bool){
    let inputs = document.querySelectorAll(".input-field > input");

    inputs.forEach(i => {
        if(bool == true){
            return i.setAttribute("disabled", "");
        }
        else{
            return i.removeAttribute("disabled");
        }
    });
}

toggle_inputs_disabled(true);

contactSwitch.addEventListener("click", () => {
    let inputs = document.querySelectorAll(".input-field > input");
    let contactWarning = document.getElementById("contact__warning");
    if(contactSwitch.checked == true){
        toggle_inputs_disabled(false);
        contactWarning.classList.add("flip-out-hor-bottom");
    }
    else{
        toggle_inputs_disabled(true);
        contactWarning.classList.remove("flip-out-hor-bottom");
        Materialize.fadeInImage('#contact__warning');
    }
});
<?php endif; ?>
</script>
