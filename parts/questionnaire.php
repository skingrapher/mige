<div id="q__fields">
    <p><?php.__("Add a response choice.", "mige"); ?></p>
    
    <input id="q__rep" type="text" name="rep" placeholder="choix de réponse">
    <button id="q__add"><span class="dashicons dashicons-plus"></span></button>
    <hr/>
<?php
if(isset($meta["reponse_choix_1"])) : ?>
    <p id="q__edit-remove"><?php _e("Edit or remove a field.", "mige"); ?></p>
<?php
endif;


// @link https://secure.php.net/manual/en/function.preg-grep.php#111673
function preg_grep_keys($pattern, $input, $flags = 0) {
    return array_intersect_key($input, array_flip(preg_grep($pattern, array_keys($input), $flags)));
}
// all params for question
$current_resp_array = get_post_meta($post->ID, "question", false);
$current_resp_array = $current_resp_array[0];
$current_resp_array = preg_grep_keys("/reponse_choix_\d+/", $current_resp_array, 0); ?>

    <div id="q__inputs">

<?php
if(count($current_resp_array) > 0) : 
    foreach($current_resp_array as $k => $v) : ?>
        <div>
            <input inputmode="text" type="text" name="question[<?php print $k; ?>]" value="<?php print $v; ?>"/>
            <button><span class="dashicons dashicons-minus"></span></button>
        </div>
<?php
    endforeach;
endif; ?>

    </div>
</div>

