<?php
/*
 * GEOCODER
 * adherent: array with rue and ville keys
 * get latitude and longitude by street and city
 */


/*
 * OPEN CAGE DATA
 * API KEY
 */

define("OCD_KEY", "6fd76aa191b149539a95c3baa769038e");

/*
 * ///---- DO NOT TOUCH CODE BELOW ----///
 */
function OCD_query($rue,$ville){
    $data = [
        "q"         => $rue." ".$ville,
        "limit"     => 1,
        "no_record" => 1,
        "language"  => "fr",
        "countrycode"       => "fr",
        "no_annotations"    => 1
    ];
    /*
     * ENCODE URL
     * WITH HTTP BUILD QUERY
     * URL = API + ENDPOINT
     */
    $endpoint = $api.http_build_query($data);
    $geocoder_url = "https://api.opencagedata.com/geocode/v1/geojson?".$endpoint."&key=".OCD_KEY;
    return $geocoder_url;
}

function get_coord($endpoint){
    // parsing url
    $endpoint = preg_match_all("/\w+=\w+(?=\&)/", $endpoint,$data); 

    foreach($data as $d) :
        str_replace("=","_",$d);
    endforeach;
        
    $data = join("_", $endpoint);

    // naming transient
    $mige_coord = "mige_coord_".$data;

    // get value of transient
    $get_coord = get_transient($mige_coord);

    if($get_coord === false) :
        $query = wp_remote_get($endpoint, [
            "user-agent"            => $_SERVER["HTTP_USER_AGENT"],
            "reject_unsafe_urls"    => true
        ]);
        set_transient($mige_coord, $query, DAY_IN_SECONDS);
    else :
        $query = $get_coord;
    endif;

    return $query;
}

/*
 * TODO
 * GEOCODING FUNCTIONALITY
 * SHOULD BE SEPARATED FROM
 * DATA VISUALIZATION TIMING
 */
function geocoder($adherent){
    $rue = $adherent->rue;
    $ville = $adherent->ville;

    /* 
     * nominatim experimental query but faster
     * sometimes returns empty array whereas returns json with data with standard query :/
     * see below
     */

    /*
    $api = "http://nominatim.openstreetmap.org/search?";

    $data = [
        "street"    => $rue,
        "city"      => $ville,
        "email"     => get_bloginfo("admin_email"),
        "limit"     => 1,
        "format"    => "json"
    ];

    $endpoint = $api.http_build_query($data);
    */

    /*
     * USE WP_REMOTE_GET
     * @link https://pippinsplugins.com/using-wp_remote_get-to-parse-json-from-remote-apis/
     */
    $endpoint = OCD_query($rue, $ville);
    $query = get_coord($endpoint);

    if( is_wp_error($query) == false){
        $response = wp_remote_retrieve_body($query);
        $json = json_decode($response);
    }

    return $json;
}

function updateCoord($marker, $adherent, $table){
    global $wpdb;

    if( empty($adherent->lat) == true || empty($adherent->long) == true ){

        $coord = geocoder($adherent);

    /* 
        // OSM FORMAT
        if(is_array($coord)){

            if(isset($coord[0])){
                $lat = $coord[0]->lat;
                $lon = $coord[0]->lon;

                // add to marker array to further display on map
                $marker["lat"] = $lat;
                $marker["lon"] = $lon;

                //update table with coordinates
                $wpdb->update(
                    $prefix.$db,
                    [
                        "lat" => $lat,
                        "long" => $lon
                    ],
                    ["id" => $adherent->id],
                    ["%f", "%f"]
                );

            }

        }
     */
        // OCD FORMAT
        if(is_object($coord)){

            if(isset($coord)){
                $features = $coord->features;
                $features = $features[0];
                $coord = $features->geometry->coordinates;
                $lat = $coord[1];
                $lon = $coord[0];

                //update table with coordinates
                $wpdb->update(
                    $table,
                    [
                        "lat" => $lat,
                        "long" => $lon
                    ],
                    ["id" => $adherent->id],
                    ["%f", "%f"]
                );

                // add to marker array to further display on map
                $marker["lat"] = $lat;
                $marker["lon"] = $lon;

                return $marker;
            }

        }
        // coordinates are not updated
        else{
            return $marker;
        }
    } // end of geocoding
    // coordinates are not updated
    else{
        return $marker;
    }
}

/*
 * MARKERS
 * prepare markers for map
 */
function markers(){
    global $wpdb;
    $prefix = $wpdb->prefix;
    $db = "participants_database";

    $mige_data_cache = wp_cache_get("mige_data_cache", "marqueurs");

    if($mige_data_cache === false) :
        // disambiguate a cached false
        wp_cache_delete("mige_data_cache", "marqueurs");

        // query database
        $query = "SELECT * FROM ".$prefix.$db." WHERE calques IS NOT NULL";

        $adherents = $wpdb->get_results($query);

        wp_cache_set("mige_data_cache", $adherents, "marqueurs", 5 * MINUTE_IN_SECONDS);
    else : 
        $adherents = $mige_data_cache;
    endif;


    // prepare array for results
    $markers = [];

    foreach($adherents as $adherent){

        preg_match_all("/\w{3,}/",$adherent->calques, $calques);
        $calques_nb = count( $calques[0] );

        $marker = [
            "nom"           => $adherent->nom,
            "prenom"        => $adherent->prenom,
            "entreprise"    => $adherent->entreprise,
            "description"   => $adherent->description,
            "lat"           => $adherent->lat,
            "lon"           => $adherent->long,
            "rue"           => $adherent->rue,
            "code_postal"   => $adherent->code_postal,
            "ville"         => $adherent->ville,
            "tel"           => $adherent->tel,
            "website"       => $adherent->website,
            "comptoir"      => $adherent->comptoir,
        ];

        // if there is layer
        if ( !empty($calques_nb) && is_int($calques_nb) == true ) {

            $marker["calques"] = array_values($calques[0]);

            $marker= updateCoord($marker, $adherent, $prefix.$db);

            // add marker to list of markers only if it has coordinates
            if( empty($adherent->lat) == false && empty($adherent->long) == false ){
                array_push($markers, $marker);
            }
        } // end of verif if adherent has a registered layer

    }// end of foreach

    // select money change rows with empty layers
    $second_query = "SELECT * FROM ".$prefix.$db." WHERE (comptoir = 'oui' AND calques = '') OR (comptoir = 'oui' AND calques IS NULL)";
    $comptoirs = $wpdb->get_results($second_query);
    foreach($comptoirs as $comptoir){
        $marker = [
            "nom"           => $adherent->nom,
            "prenom"        => $adherent->prenom,
            "entreprise"    => $adherent->entreprise,
            "description"   => $adherent->description,
            "lat"           => $adherent->lat,
            "lon"           => $adherent->long,
            "rue"           => $adherent->rue,
            "code_postal"   => $adherent->code_postal,
            "ville"         => $adherent->ville,
            "tel"           => $adherent->tel,
            "website"       => $adherent->website,
            "comptoir"      => $adherent->comptoir,
            "calques"       => [],
        ];

        if ( $marker["comptoir"] == "oui" ) {

            $marker= updateCoord($marker, $adherent, $prefix.$db);

            // add marker to list of markers only if it has coordinates
            if( empty($adherent->lat) == false && empty($adherent->long) == false ){
                array_push($markers, $marker);
            }
        } // end of verif if money change rows 
    }

    // prepare JSON Object
    $json = json_encode(array_values($markers));

    return $json;
}

// MISE EN CACHE TEMPORAIRE
function mige_markers_transient(){
    $mige_transient = "mige_prestataires";
    $t = get_transient($mige_transient); 
    $json = markers();

    if( $t === false) :
        set_transient($mige_transient, $json, 10 * MINUTE_IN_SECONDS);
        return $json;

    elseif($t  != $json) :
        delete_transient($mige_transient);
        set_transient($mige_transient, $json, 10 * MINUTE_IN_SECONDS);
        return $json;

    else :
        return $t;
    endif;
}

//$json = mige_markers_transient();
$json = markers();
?>
<script>

// GET JSON DATA
let markers = <?php print $json; ?>;

/*
 * FUNCTIONS
 */

/* PIN A MARKER ON MAP FOR EACH PARTICIPANT 
 * marqueur: JSON object from database
 * carte: Leaflet map
 */
function pin(marqueur, carte){

    // nom de l'entreprise
    if(marqueur.entreprise != undefined){
        var entreprise = "<h3>"+marqueur.entreprise+"</h3>"
    }
    else{
        var entreprise = null 
    }

    // identité de l'adhérent : nom + prénom
    if(marqueur.nom != undefined && marqueur.prenom != undefined){
        var identity = "<h4>"+marqueur.prenom+" "+marqueur.nom.toUpperCase()+"</h4>";
    }
    else{
        var identity = null
    }

    // numéro de tél
    if(marqueur.tel != ""){
        var tel = "<dt><span class=\"material-icons tiny\" aria-hidden=\"true\">phone</span><span>Tél.</span></dt><dd><a href=\"tel:"+marqueur.tel.replace(/\s+/g,'')+"\">"+marqueur.tel+"</a></dd>"
    }
    else{
        var tel = null
    }
    
    // site web 
    if(marqueur.website != ""){
        var website = "<dt><span class=\"material-icons tiny\" aria-hidden=\"true\">local_offer</span><?php _e("Website","mige"); ?></dt><dd>"+marqueur.website+"</dd>"
    }
    else{
        var website = null
    }

    if(website != null || tel != null){
        var dl = "<dl>";
    
        if(website != null){
            dl += website
        }
        if(tel != null){
            dl += tel 
        }

        dl += "</dl>";
    }
    else{
        dl = null
    }
    
    let message = "";

    if(entreprise != null){
        message += entreprise
    }

    if(identity != null){
        message += identity
    }

    message += "<address>"+marqueur.rue+"<br/>"+marqueur.ville;

    if(dl != null){
        message += dl
    }

    message += "</address>";

    let pin = L.marker([marqueur.lat, marqueur.lon]).addTo(carte).bindPopup(message);

    return pin
}

/*
 * GEOLOCATION ON MAP
 * position: object from
 * carte: Leaflet map
 */
function youAreHere(position, carte){
    
    let lat = position.coords.latitude;
    let lon = position.coords.longitude;
    
    let here = L.divIcon({
        html: "<svg width='24px' height='24px' viewBox='0 0 24 24'><path fill='red' d='M12,8A4,4 0 0,1 16,12A4,4 0 0,1 12,16A4,4 0 0,1 8,12A4,4 0 0,1 12,8M3.05,13H1V11H3.05C3.5,6.83 6.83,3.5 11,3.05V1H13V3.05C17.17,3.5 20.5,6.83 20.95,11H23V13H20.95C20.5,17.17 17.17,20.5 13,20.95V23H11V20.95C6.83,20.5 3.5,17.17 3.05,13M12,5A7,7 0 0,0 5,12A7,7 0 0,0 12,19A7,7 0 0,0 19,12A7,7 0 0,0 12,5Z'/></svg>"
    });

    let pin = L.marker([lat, lon], {icon: here}).addTo(carte).bindTooltip("<?php _e("You are here.", "mige"); ?>").openTooltip();
    
    return pin
}

/*
 * ADD LAYER GROUPS ON MAP
 * groups: array of layers to add as layerGroups 
 * layers: JSON object of named layers
 */
function addLayerGroups(groupes, carte){
    /*
     * ADD GROUPS
     * GROUP MARKERS ON SPECIFIC LAYERS ON MAP
     */
    let change = L.layerGroup().addTo(carte);
    
    let calques = {
        "comptoirs": change
    };

    groupes.map(calque => {
        calques[calque] = L.layerGroup()
    })

    /*
     * ADD ALL LAYERS TO MAP
     * @link http://leafletjs.com/reference-1.3.0.html#control-layers
     */  
    L.control.layers(
        calques,
        null,
        {
            collapsed: false,
            sortLayers: false,
            hideSingleBase: true
        }).addTo(carte);

    return calques;
}

function layers(markers){

    markers.map( m => {
        let layers = Object.values(m.calques);
        console.log(layers);
        return layers;
    })
}

/*
 * OPEN STREET MAP TILE
 */
const OSM_URL = "https://\{s\}.tile.openstreetmap.fr/osmfr/\{z\}/\{x\}/\{y\}.png";

/*
 * DEFINE ZONE ON MAP
 */
let carte = L.map("carte", {
    zoom: 9,
    center: [ 46.0661, 1.8086],
    attributionControl: false,
    scrollWheelZoom: false,
    renderer: L.svg()
});

/* 
 * OSM LAYER
 * DISPLAY TILE ON MAP
 * ADD MAP ZONE
 */
let osmTile = L.tileLayer(OSM_URL).addTo(carte);

/*
 * BASE LAYERS
 */
let listOfLayers = []

Object.keys(markers).map(k =>{
    let layers = markers[k].calques;
    listOfLayers = listOfLayers.concat(layers);
    // make each layer unique and sort them all
    listOfLayers = listOfLayers.filter( (layer, index) => listOfLayers.indexOf(layer) === index ).sort();
})

// convert array of layers into JSON object with layerGroups
listOfLayers = addLayerGroups(listOfLayers, carte);

Object.keys(markers).map(k =>{
    let m = markers[k];

    if(m.comptoir == "oui"){
        pin(m, listOfLayers["comptoirs"])
    }

    m.calques.map( calque => {
        pin(m, listOfLayers[calque])
    });
});
  
/*
 * GEOLOCATION
 */
/*  if ("geolocation" in navigator) {
  navigator.geolocation.getCurrentPosition(pos => {
    youAreHere(pos)
});
}*/
</script>

