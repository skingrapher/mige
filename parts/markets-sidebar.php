<?php
/*
 * MARKETS
 */

// types de commerce pour les marchés
$commerces = get_terms([
    "taxonomy"      => "commerce",
    "hide_empty"    => true
]);

// places de marché communales
$marketplaces = get_terms([
    "taxonomy"      => "lieu",
    "hide_empty"    => true
]);

if(!empty($commerces) || !empty($marketplaces)) : ?>
    <h2><?php _e("Markets", "mige"); ?></h2>
<?php
endif;

if(!empty($commerces)) : 
    foreach($commerces as $commerce) :
        $term = $commerce->name;
        $term_link = get_term_link($commerce->slug, "commerce");
        $term_markup = "<span class=\"chip\"><a href=\"%s\">%s</a></span>";
        printf($term_markup, $term_link, $term);
        
    endforeach;
endif;

if(!empty($marketplaces)) : 
    foreach($marketplaces as $place) :
        $term = $commerce->name;
        $term_link = get_term_link($commerce->slug, "lieu");
        $term_markup = "<span class=\"chip\"><a href=\"%s\">%s</a></span>";
        printf($term_markup, $term_link, $term);
        
    endforeach;
endif;

