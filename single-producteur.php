<?php get_header(); ?>

<div class="row container section">
  <main class="col s12 m9">

<?php

$txtarea_max_length = 200;

while (have_posts()) : the_post();

    $post_id = get_the_ID();

?>
        <article>
<?php
    the_title("<h3>", "</h3>");

    // if has thumbnail
    if (has_post_thumbnail()) :
    
        // available formats: thumbnail, medium, medium_large, large, full
        // @link https://www.smashingmagazine.com/2016/09/responsive-images-in-wordpress-with-art-direction/
        $thumbnail_format = "thumbnail"; // possible values: thumbn
        $thumbnail_id = get_post_thumbnail_id($post_id);
        $thumbnail_src = wp_get_attachment_image_src($thumbnail_id, "thumbnail");
        $thumbnail_srcset = wp_get_attachment_image_srcset($thumbnail_id, "thumbnail");
        $thumbnail_sizes = wp_get_attachment_image_sizes($thumbnail_id, "thumbnail");
        $thumbnail_alt = get_post_meta($thumbnail_id, "_wp_attachment_image_alt", true);

?>

            <img class="lazyload blur responsive-img"
                srcset="<?php print  esc_attr($thumbnail_srcset); ?>"
                data-srcset="<?php print  esc_attr($thumbnail_srcset); ?>"
                src="<?php print  esc_attr($thumbnail_src); ?>"
                alt="<?php print  esc_attr($thumbnail_alt); ?>"
                sizes="<?php print esc_attr($thumbnail_sizes); ?>"/>

<?php
endif;
the_content();
?>
            <p><?php _e("Posted on ", "mige"); ?><time datetime="<?php the_time("c"); ?>" class="small"><?php the_date("j F Y"); ?></time></p>
            <p><?php the_terms($post_id, "producteur", "<span class=\"chip\">", "</span><span class=\"chip\">", "</span>"); ?></p>
        </article>

<?php
endwhile; // end of loop
?>

  </main>

<?php get_sidebar(); ?>

</div>

<?php
get_footer();

