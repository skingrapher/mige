<?php
/**
 * @package mige
 */

get_header(); 

// strip the more tag from display
global $more;
$more = -1;

// front-page
if (is_front_page()) : ?>

    <h2>Où monnayer&nbsp;?</h2>
    <div id="carte"></div>

<?php elseif( is_404() ) : ?>

<p class="flow-text center-align"><?php _e("Page not found.","mige"); ?></p>

<?php else :

    while (have_posts()) : the_post();

        $post_id = get_the_ID();

        // single post
        if ( is_single() ) :
            the_title("<h2>", "</h2>"); ?>

            <article>
            <?php
        
            // if has thumbnail
            if (has_post_thumbnail()) :
            
                // available formats: thumbnail, medium, medium_large, large, full
                // @link https://www.smashingmagazine.com/2016/09/responsive-images-in-wordpress-with-art-direction/
                $thumbnail_format = "thumbnail"; // possible values: thumbn
                $thumbnail_id = get_post_thumbnail_id($post_id);
                $thumbnail_src = wp_get_attachment_image_src($thumbnail_id, "thumbnail");
                $thumbnail_srcset = wp_get_attachment_image_srcset($thumbnail_id, "thumbnail");
                $thumbnail_sizes = wp_get_attachment_image_sizes($thumbnail_id, "thumbnail");
                $thumbnail_alt = get_post_meta($thumbnail_id, "_wp_attachment_image_alt", true);

            ?>

                <img class="lazyload blur responsive-img"
                    srcset="<?php print  esc_attr($thumbnail_srcset); ?>"
                    data-srcset="<?php print  esc_attr($thumbnail_srcset); ?>"
                    src="<?php print  esc_attr($thumbnail_src); ?>"
                    alt="<?php print  esc_attr($thumbnail_alt); ?>"
                    sizes="<?php print esc_attr($thumbnail_sizes); ?>"/>

            <?php
            endif;

            the_content(); ?>

                <p><?php _e("Posted on ", "mige"); ?><time datetime="<?php the_time("c"); ?>" class="small"><?php the_date("j F Y"); ?></time></p>
                <p><?php the_tags("<span class=\"chip\">", "</span><span class=\"chip\">", "</span>"); ?></p>
            <?php
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
            ?>
            </article>

<?php

        // homepage (feed of posts)
        elseif( is_home() ) :

            get_template_part( 'parts/card' );

        endif; // single post or homepage -- end of conditional statement 
    endwhile; // end of loop
endif; // front-page or not -- end of conditional statement 
?>

  </main>

<?php get_sidebar(); ?>

</div>

<?php
get_footer();
