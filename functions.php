<?php
/**
 * MIGE
 * functions and definitions
 * @package mige
 */

/*
 * USED FOR:
 * CONTACT PAGE
 * QUESTIONNAIRES
 */
if ( ! function_exists( 'randomCaptcha' ) ) :
    function randomCaptcha(){
        $captcha = [];
        for($i=0;$i<4;$i++){
            $val = strval(rand(0,9));
            array_push($captcha, $val);
        }
        $captcha = join("",$captcha);
        return $captcha;
    }
endif;

/*
 * CREATE TABLE
 * FOR RESULTS
 * FROM QUESTIONNAIRE
 */
if ( ! function_exists( 'questionnaire_create_table' ) ) :
    function questionnaire_create_table(){
        global $wpdb;
        $table = $wpdb->prefix."questionnaire_results";


        //ip varchar(45) NOT NULL,
        $query = "CREATE TABLE $table (
        id mediumint(6) AUTO_INCREMENT NOT NULL,
        post_id varchar(200) NOT NULL,
        choix varchar(8) NOT NULL,
        rep varchar(300),
        rep_alt varchar(200),
        PRIMARY KEY  (id),
        KEY post_id (post_id)
        ) CHARACTER SET utf8;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        // @link https://codex.wordpress.org/Creating_Tables_with_Plugins
        dbDelta( $query );
    }

    // when theme is activated, run function
    add_action("after_switch_theme", "questionnaire_create_table");
endif;

/*
 * ADD RESPONSES TO TABLE
 * FOR QUESTIONNAIRE
 * @see single-questionnaire.php
 */
if ( ! function_exists( 'questionnaire_add_resp' ) ) :
    function questionnaire_add_resp($postid, $choice, $posted){
        global $wpdb;
        $table = $wpdb->prefix."questionnaire_results";

        $row = [
            "post_id"   => $postid,
            //"ip"        => $_SERVER["REMOTE_ADDR"],
            "choix"     => $choice
        ];

        // questionnaire choix unique
        if( isset( $posted["rep"] ) && $choice == "unique" ){
            $row[ "rep" ] = $posted["rep"];
        }
        // questionnaire choix multiple
        elseif( isset( $posted["rep"] ) && $choice == "multiple" ){
            $reps = [];
            // put each checked value in array
            foreach($posted["rep"] as $rep){
                array_push($reps, $rep);
            }
            // join them in a string
            $reps = join(",", $reps);
            // prepare to add to table
            $row["rep" ] = $reps;
        }
        // autre réponse
        if( isset( $posted["rep_alt"] ) ){
            $row[ "rep_alt" ] = $posted["rep_alt"];
        }

        // array of formats to be mapped to each of the values in row 
        $format = array_fill(0, count($row), "%s");

        // insert row into table
        $wpdb->insert($table, $row, $format);
    }
endif;

/*
 * TOOL FOR CONTACT FORM
 * @see parts/contact.php
 */
if ( ! function_exists( 'poster' ) ) :

    function poster($expediteur, $courriel, $message){
        /*
         * $expediteur: texte
         * $courriel: mel valide
         * $message: text
         */

        // COURRIEL DE L'EXPEDITEUR
        // @link https://fightingforalostcause.net/content/misc/2006/compare-email-regex.php
        $pattern = "/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i";
        $validation = preg_match($pattern, $courriel);
        if($validation == 1){
            // paranoid
            $courriel = filter_var($courriel, FILTER_SANITIZE_EMAIL);


            // OBJET DU MESSAGE
            $sujet = "[".get_bloginfo('name')."] nouveau message";

            // NOM DE L'EXPEDITEUR
            $expediteur = filter_var( trim($expediteur), FILTER_SANITIZE_STRING);

            $entetes = "From: ".$expediteur." <".$courriel.">\r\n".
                "X-Mailer: PHP/".phpversion()."\r\n".
                "Content-Type: text/plain; charset=iso-8859-1";

            // MESSAGE
            // sanitize text
            // @link https://secure.php.net/manual/fr/filter.filters.sanitize.php#118186
            $message = strip_tags($message);
            // RFC text length must be less than 70 characters
            // https://secure.php.net/manual/fr/function.mail.php
            $message = wordwrap($message, 70, "\r\n");

            // DESTINATAIRE : ADMIN
            $destinataire = get_option("admin_email");

            $envoi = mail($destinataire, $sujet, $message, $entetes);

            if($envoi == 1){
                print "<p class=\"flow-text section\">".__("Your email has been sent successfully to the webmaster of this site.", "mige")."</p>";
            }
            else{
                print "<p class=\"flow-text section\">".__("An error occurred. We couldn't send your message. Try one more time.", "mige")."</p>";
            }
        }
        
        else{
            print "<p class=\"flow-text section\">".__("Your email is not valid. Please, fix it.", "mige")."</p>";
        }

        return $envoi;
    }

endif;

if ( ! function_exists( 'mige_setup' ) ) :

	function mige_setup() {

		load_theme_textdomain( "mige", get_template_directory() . "/lang" );

		add_theme_support( "title-tag" );
		add_theme_support( "automatic-feed-links" );
		add_theme_support( "post-thumbnails", ["post", "page", "questionnaire"] );

		register_nav_menus( array(
            // both in header
			"main" => esc_html__( "principal", "mige" ),
			"drawer" => esc_html__( "latéral", "mige" ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'caption',
		) );

		add_theme_support( 'custom-background', apply_filters( 'mige_custom_background_args', array(
			'default-color' => 'f4f4f4',
			'default-image' => '',
		) ) );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'custom-logo', array(
			'height'      => 50,
			'width'       => 50,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}

endif;

add_action( 'after_setup_theme', 'mige_setup' );

/*
 * CUSTOM POST TYPES
 * must be called AFTER after_setup_theme hook AND BEFORE admin_menu hook
 * @link https://codex.wordpress.org/Post_Types
 */

require get_template_directory() . "/parts/custom-post-functions.php";

add_action("init", "mige_custom_post_types");
add_action("add_meta_boxes", "questionnaire_box");
add_action("save_post", "save_question_meta");
add_action("after_delete_post", "del_question_meta");

/*
 * WIDGETS
 */
function mige_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'barre latérale droite', 'mige' ),
		'id'            => 'right_sidebar',
		'description'   => esc_html__( 'Ajoutez vos widgets ici', 'mige' ),
		'before_widget' => '<section id="%1$s" class="%2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'mige_widgets_init' );

/*
 * REMOVE WORDPRESS JQUERY VERSION
 * @link https://github.com/jonathanbell/remove-jquery-wordpress-plugin
 * @author Jonathan Bell
 */

// Remove jQuery Migrate.
function remove_jquery_dequeue_jquery_migrate($scripts) {
  if (is_admin()) {
    return;
  }
  if (!empty($scripts->registered['jquery'])) {
    $jquery_dependencies = $scripts->registered['jquery']->deps;
    $scripts->registered['jquery']->deps = array_diff($jquery_dependencies, array('jquery-migrate'));
  }
}

add_action('wp_default_scripts', 'remove_jquery_dequeue_jquery_migrate');

// Remove jQuery.
// https://stackoverflow.com/a/27048128/1171790
function remove_jquery() {
  if (is_admin()) {
    return;
  }
  wp_dequeue_script('jquery');
  wp_deregister_script('jquery');
}
add_filter('wp_enqueue_scripts', 'remove_jquery', PHP_INT_MAX - 1);

/*
 * MIGE SCRIPTS
 * CSS + JS
 */
function mige_async_scripts($tag, $handle, $src){

    if (in_array($handle, ["lazysizes"])) :
        return '<script async src="'.$src.'"></script>'."\n";
    endif;

    if (in_array($handle, ["leafletjs"])) :
        return '<script crossorigin src="'.$src.'" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="></script>'."\n";
    endif;

    if (in_array($handle, ["last_jquery"])) :
        return '<script crossorigin src="'.$src.'" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="></script>'."\n";
    endif;

    return $tag;
}

add_filter("script_loader_tag", "mige_async_scripts", 10, 3);

function mige_leaflet_integrity($tag, $handle, $href){

    if (in_array($handle, ["leaflet"])) :
        return '<link media="screen" href="'.$href.'" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin rel="stylesheet">'."\n";
    else :
        return $tag;
    endif;
}

add_filter("style_loader_tag", "mige_leaflet_integrity", 10, 3);

function mige_scripts() {
    /* LEAFLET JS
    @link http://leafletjs.com/
    JavaScript library for interactive maps
    */
    global $post;

    if(is_front_page()) :
        /*
        wp_enqueue_style( 'leaflet', get_template_directory_uri() . "/css/leaflet.css", [], "1.3.1", "all" );
        wp_enqueue_script( "leafletjs", get_template_directory_uri() . "/js/leaflet.js", [], "1.3.1", true );
         */
        wp_enqueue_style( 'leaflet', "https://unpkg.com/leaflet@1.3.1/dist/leaflet.css", [], null, "all" );
        wp_enqueue_script( "leafletjs", "https://unpkg.com/leaflet@1.3.1/dist/leaflet.js", [], null, true );
    endif;

    // STYLE.CSS
	wp_enqueue_style( 'mige', get_stylesheet_uri() );

    // MATERIALIZE CSS
    /*
    wp_enqueue_script( "last_jquery", get_template_directory_uri() . "/js/jquery.js", [], "3.3.1", true );
    wp_enqueue_script( "materialize", get_template_directory_uri() . "/js/materialize.min.js", ["last_jquery"], "0.100.2", true );
     */
    //wp_enqueue_script( "last_jquery", "https://code.jquery.com/jquery-3.3.1.min.js", [], null, true );
    //wp_enqueue_script( "materialize", "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js", ["last_jquery"], null, true );

    /* LAZYSIZES JS
     * @link https://github.com/aFarkas/lazysizes
     * High performance and SEO friendly lazyloader for images
     */
    //wp_enqueue_script( "lazysizes", get_template_directory_uri() . "/js/lazysizes.js", [], "4.0.1", false );


    if(get_post_type($post->ID) == "questionnaire") :
        // D3 FOR VISUAL REPRESENTATION OF RESLULTS
        //wp_enqueue_script( "d3", get_template_directory_uri() . "/js/d3.js", [], "4.13.0", true );
        wp_enqueue_script( "d3", "https://unpkg.com/d3@4.13.0/build/d3.min.js", [], null, true );
        wp_enqueue_script( "questionnaire-results", get_template_directory_uri() . "/js/questionnaire-results.js", ["d3"], null, true );
    endif;
}

add_action( 'wp_enqueue_scripts', 'mige_scripts' );

/*
 * QUESTIONNAIRE
 * JS SCRIPTS
 */
function mige_questionnaire_scripts(){
    global $post;
    if( ( isset($_GET["post"]) && get_post_type($post->ID) == "questionnaire" ) || ( isset($_GET["post_type"]) && $_GET["post_type"] == "questionnaire" ) ) :
        wp_enqueue_script( "questionnaire", get_template_directory_uri() . "/js/questionnaire.js", [], null, true );
        // D3 FOR VISUAL REPRESENTATION OF RESLULTS
        //wp_enqueue_script( "d3", get_template_directory_uri() . "/js/d3.js", [], "4.13.0", true );
        wp_enqueue_script( "d3", "https://unpkg.com/d3@4.13.0/build/d3.min.js", [], null, true );
        wp_enqueue_script( "questionnaire-results", get_template_directory_uri() . "/js/questionnaire-results.js", ["d3"], null, true );
    endif;
}

add_action("admin_enqueue_scripts", "mige_questionnaire_scripts");

/*
 * CLEAN UP WP HEAD TAG
 */
remove_action("wp_head", "rsd_link");
remove_action("wp_head", "wlwmanifest_link");
remove_action("wp_head", "wp_site_icon");
remove_action("wp_head", "rest_output_link_wp_head");

// REMOVE REST API LINK
function mige_disable_emoji(){
remove_action("wp_head", "print_emoji_detection_script", 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
}

add_action("init", "mige_disable_emoji");

// REMOVE ADMIN BAR FROM FRONT FACING PAGES
add_filter('show_admin_bar', '__return_false');
// REMOVE ID FROM MENU ITEM
add_filter('nav_menu_item_id', '__return_false');

/*
 * RESPONSIVE VIDEO
/* make media elements responsive
/* BAD HACK
 */
function set_responsive_embed($el,$url,$attr, $id){
    return "<div class=\"responsive-video\">".$el."</div>";
}
// priority: 10
// number of arguments: 4
add_filter("embed_oembed_html", "set_responsive_embed", 10, 4);


/*
 * ADD CUSTOM POSTS
 * TO LOOP IN
 * HOMEPAGE
 */
if ( ! function_exists( 'add_custom_posts_to_loop' ) ) :
    function add_custom_posts_to_loop($query){
        if(is_home() && $query->is_main_query()) :
            $query->set("post_type", ["post", "questionnaire"]);
        endif;

        return $query;
    }

    add_action("pre_get_posts", "add_custom_posts_to_loop");
endif;

/*
 * REMOVE ENTRIES
 * FROM ADMIN MENU
 */
function mige_remove_admin_pages(){
    // remove themes entry
    remove_menu_page("themes.php");
    // remove categories from posts entry
    remove_submenu_page("edit.php", "edit-tags.php?taxonomy=category");
}

add_action("admin_menu", "mige_remove_admin_pages");

/*
 * REMOVE CATEGORY METABOXES
 */
function mige_remove_cat(){
    remove_meta_box("categorydiv", "post", "side");
    remove_meta_box("categorydiv", "page", "side");
}

add_action("admin_init", "mige_remove_cat");

/*
 * ADMIN FOOTER 
 */
function mige_admin_footer(){
    $repo = "http://github.com/skrypte/mige";
    $author_url = "http://www.skingrapher.com";
    sprintf('Code source du thème <a href="%s" target="_blank">Mige</a> sous licence GPL3, disponible sur <a href="%s" target="_blank">github</a> et réalisé par <a href="%s">skingrapher</a>.', $repo, $repo, $author_url);
}

add_filter('admin_footer_text', 'mige_admin_footer');

/*
 * CUSTOM LOGIN PAGE
 */
function sk_url(){
    return "https://www.skingrapher.com";
}

add_filter("login_headerurl", "sk_url");

function sk_title(){
    return "Mige Theme by skingrapher";
}

add_filter("login_headertitle", "sk_title");

function sk_login_style(){
    wp_enqueue_style( 'sk-login', get_template_directory_uri() . "/css/login.css", [], null, "screen" );
}

add_action("login_head", "sk_login_style");

// change dns-prefetch for preconnect
function mige_preconnect($hints, $relation){
    if ($relation === "preconnect") :
        $hints = [
            "//unpkg.com/",
            //"//code.jquery.com/",
            //"//cdnjs.cloudflare.com/"
        ];
    elseif ($relation === "dns-prefetch") :
        $hints = [];
    endif;

    return $hints;
}
add_filter("wp_resource_hints", "mige_preconnect", 10, 2);
