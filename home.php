<?php
// strip the more tag from display
get_header();

global $more;
$more = -1;

if (have_posts() && is_home()) :

    while (have_posts()) : the_post();

        $post_id = get_the_ID();

        get_template_part( 'parts/card' );

    endwhile; // end of loop

    get_template_part( 'parts/pagination' );

endif;
?>

  </main>

<?php get_sidebar(); ?>

</div>

<?php
get_footer();
