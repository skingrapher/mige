<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mige
 */

get_header(); ?>

<div class="row container section">
  <main class="col s12 m9">

		<?php
		if ( have_posts() ) :

            the_archive_title( '<h2>', '</h2>' );
            the_archive_description( "<h3>", "</h3>" );

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'parts/card' );

			endwhile;

            get_template_part( 'parts/pagination' );

		else : ?> 

            <p><?php _e("Nothing found", "mige"); ?></p>

        <?php

		endif; ?>

  </main>

<?php get_sidebar(); ?>

</div>

<?php
get_footer();
