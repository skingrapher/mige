<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mige
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<section>

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
        <h3><?php _e("Comments", "mige") ?></h3>

		<ol>
        <?php

        $comment_args = [
            "reverse_top_level" => true,
            "reverse_children"  => false,
            "format"            => "html5",
            "type"              => "comment",
            "style"             => "ol",
            "avatar_size"       => 24
        ];

        wp_list_comments($comment_args);

        ?>
		</ol>

		<?php the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) : ?>
			<p class="red-text"><?php _e( 'Comments are closed.', 'mige' ); ?></p>
		<?php
		endif;

    endif;

    ?>

    <div class="row">
    <?php


    // Comparing E-mail Address Validating Regular Expressions
    // @link https://fightingforalostcause.net/content/misc/2006/compare-email-regex.php
    $email_pattern = "^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$";

    $author_field = '<div class="input-field col s12 m6"><input name="author" maxlength="245" required autocomplete="username" placeholder="Jean Bonnot" type="text" id="comment__author" class="validate"/><label for="comment__author">'.__("Name","mige").'</label></div>';
    $email_field = '<div class="input-field col s12 m6"><input name="email" maxlength="100" required autocomplete="email" pattern="'.$email_pattern.'" placeholder="jean.bonnot@monboucher.fr" type="email" id="comment__email" class="validate"/><label for="comment__email">'.__("Email","mige").'</label></div>';
    $url_field = '<div class="input-field col s12 m6"><input name="url" maxlength="200" required autocomplete="url" placeholder="http://monboucher.fr" type="url" id="comment__url" class="validate"/><label for="comment__url">'.__("Website","mige").'</label></div>';

    $mentions_legales_permalink = get_permalink("mentions-legales");

    // @link https://developer.wordpress.org/reference/functions/comment_form/
    $comment_form_args = [
        "fields"                => [
            "author"    => $author_field,
            "email"     => $email_field,
            "url"       => $url_field
        ],
        "format"                => "html5",
        "action"                => "/wp-comments-post.php",
        "title_reply"           => __("Leave a comment", "mige"),
        "title_reply_before"    => "<h4>", 
        "title_reply_after"     => "</h4>",
        "cancel_reply_before"   => '<small>',
        "cancel_reply_after"    => '</small>',
        "cancel_reply_link"     => '<span class="material-icons tiny" aria-hidden="true">cancel</span>'._x("Cancel reply", "mige"),
        "class_form"            => "col s12",
        "label_submit"          => __("Post comment", "mige"),
        "class_submit"          => "btn waves-effect waves-light",
        "submit_button"         => '<div class="col s12"><button id="%2$s" class="%3$s" type="submit">%4$s<span class="material-icons right" aria-hidden="true">send</span><span class="screen-reader-text">'._x("Send", "mige").'</span></button></div>',
        "comment_field"         => '<div class="input-field col s12"><textarea id="comment__txt" class="materialize-textarea" name="comment" aria-required="true"></textarea><label for="comment__txt">'.__("Your comment","mige").'</label>',
        "comment_notes_before"  => 'Votre adresse courriel sera enregistrée. Pour en savoir plus, voyez <a href"'.$mentions_legales_permalink.'">mentions légales</a>.',
        "must_log_in"           => __("You must login to comment.", "mige"),
    ];

	comment_form($comment_form_args);

    ?>
    </div>

</section>
