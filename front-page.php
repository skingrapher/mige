<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mige
 */

// strip the more tag from display
global $more;
$more = -1;

get_header();

// front-page
if (is_front_page()) : ?>

    <h2>Où monnayer&nbsp;?</h2>
    <div id="carte"></div>

<?php
endif;
?>

  </main>

<?php get_sidebar(); ?>

</div>

<?php
get_footer();

