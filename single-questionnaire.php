<?php get_header(); ?>

<div class="row container section">
  <main class="col s12 m9">

<?php

$txtarea_max_length = 200;

while (have_posts()) : the_post();

    $post_id = get_the_ID();
    $meta = get_post_meta($post_id, "question", true);

    // if response was sent
    // check if there is data to store
    // warning if empty

    if(isset($_POST["_qn"])) :
        $nonce = wp_verify_nonce($_POST["_qn"], "validation");

        if($nonce != false) :

            // ----- BEGINS POST VAR VERIF -----
            if(isset($_POST["rep"]) || isset($_POST["rep_alt"]) || isset($_POST["multirep"])) :

                    if(isset($meta["choix"])) :

                        $posted = [];

                        // it there is a response only from radio selection
                        if(!empty($_POST["rep"])) :
                            $posted["rep"] = $_POST["rep"];
                        endif;

                        // if there is one or more responses from checkbox
                        if(!empty($_POST["multirep"])) :
                            $posted["rep"] = $_POST["multirep"];
                        endif;

                        if(!empty($_POST["rep_alt"])) :
                            $posted["rep_alt"] = $_POST["rep_alt"];
                        endif;

                        if(!empty($posted)) :
                            questionnaire_add_resp($post_id, $meta["choix"], $posted); ?>

        <p class="flow-text green-text"><?php _e("Your answer has been registered.", "mige"); ?></p>

<?php
                        endif;
                
                    endif; // end of verif if meta choix exists

            endif;
            // ----- END OF VAR VERIF -----
                        
        else : ?>

        <p class="flow-text red-text darken-4"><?php _e("Your answer was empty. Nothing has been registered.", "mige"); ?></p>

<?php
        endif;
    endif; // end of interact with db

    $choices = get_post_meta($post_id, "question", false);
    // @link https://secure.php.net/manual/en/function.preg-grep.php#111673
    $choices = array_intersect_key($choices[0], array_flip(preg_grep("/reponse_choix_\d+/", array_keys($choices[0]), 0)));

?>
        <section class="container">
            <h2><?php _e("Questionnaire", "mige"); ?></h2>
<?php
    the_title("<h3>", "</h3>");
?>
            <form method="POST" action="<?php the_permalink(); ?>" class="row">

<?php

    // NONCE FIELDS
    wp_nonce_field("validation", "_qn");


    if(isset($meta["choix"])){
        switch($meta["choix"]){
        case "unique":
            if($choices > 0){
                foreach($choices as $k => $v){ ?>

                <div>
                    <input name="rep" value="<?php print $v; ?>" type="radio" class="with-gap" id="<?php print $k; ?>">
                    <label for="<?php print $k; ?>"><?php print $v; ?><label>
                </div>
<?php
                }
            }
            break;

        case "multiple":
            if($choices > 0){
                foreach($choices as $k => $v){ ?>

                <div>
                    <input name="multirep[]" value="<?php print $v; ?>" type="checkbox" class="filled-in" id="<?php print $k; ?>">
                    <label for="<?php print $k; ?>"><?php print $v; ?><label>
                </div>
<?php
                }
            }
            break;
        }
    }

?>
                <!-- OTHER RESPONSE -->
                <div class="input-field col s12">
                    <textarea maxlength="<?php print $txtarea_max_len; ?>" id="other_response" class="materialize-textarea" name="rep_alt"></textarea>
                    <label for="other_response"><?php _e("Autre", "mige"); ?></label>
                </div>

                <!-- CAPTCHA -->
                <div class="input-field col s12">
                    <input inputmode="numeric" class="validate" name="captcha" id="q__captcha" required type="text" pattern="^\d{4}" value="" maxlength="4"/>
                    <label for="q__captcha"><span class="material-icons small" aria-hidden="true">label_outline</span><?php _e("Enter captcha code: ", "mige"); ?><span id="captchacode"><?php $captcha = randomCaptcha(); print $captcha; ?></span></label>
                    <button type="button" id="renewcaptcha" class="right waves-effect waves-light btn-floating">
                        <span class="material-icons small" aria-hidden="true">autorenew</span>
                        <span class="screen-reader-text"><?php _e("Renew", "mige"); ?></span>
                    </button>
                </div>

                <!--
                SUBMIT
                ------
                FF BUG
                @link https://developer.mozilla.org/fr/docs/Web/HTML/Element/Button
                @link https://bugzilla.mozilla.org/show_bug.cgi?id=654072 
                -->
                <button disabled autocomplete="off" type="submit" id="q__submit" class="disabled btn waves-effect waves-light"><?php _e("répondre", "mige"); ?></button>
            </form>
            
            <p><small>La demande de code sert de filtre antispam.<br/>Les réponses enregistrées sont anonymisées. Aucune donnée personelle n'est récupérée.</small></p>
        </section>

        <section>
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header"><span class="material-icons" aria-hidden="true">pie_charte</span><?php _e("Results", "mige"); ?></div>
                    <div class="collapsible-body">
                    <?php

        function questionnaire_display_results(){
            global $wpdb;
            global $post;
            $meta = get_post_meta($post->ID, "reponses", true);

            ////---------- QUESTIONNAIRE ON EDIT PAGE ONLY ----------////
            $query = "SELECT choix, rep, rep_alt FROM ".$wpdb->prefix."questionnaire_results WHERE post_id = ".$post->ID;
            $result = $wpdb->get_results($query, ARRAY_A);
            $reps = array_column($result, "rep");

            // IF TABLE IS EMPTY
            if( empty( count($reps) ) ) : ?>
                <p><?php _e("No one has answered this questionnaire.", "mige"); ?></p>
            <?php
            // IF ANY ANSWER TO QUESTIONNAIRE
            // ADD CANVAS FOR VISUAL REPRESENTATION
            // OF RESULTS
            else : ?>

            <div id="results__view"></div>

            <?php

                // RESULTS FOR UNIQUE CHOICE QUESTIONNAIRE
                $choice = array_column($result, "choix");
                $choice = $choice[0];

                $votes = []; 

                if($choice == "unique") :
                    $reps = array_filter($reps, function($val){ return !is_null($val); });

                    $counts = array_count_values($reps);

                    foreach($counts as $rep => $count) :
                        $vote = sprintf('{ "rep": "%s", "votes": %d}', $rep, $count);
                        array_push($votes, $vote);
                    endforeach;

                // RESULTS FOR MULTIPLE CHOICE QUESTIONNAIRE
                elseif($choice == "multiple") :
                    foreach($reps as $rep) :
                        // explode all responses in string
                        $rep = explode(",", $rep);

                        // put in each item of array in votes array
                        array_map(
                            function ($r) {
                                array_push($votes, $r);
                            },
                            $rep
                        );
                    endforeach;

                    // count each response
                    // returns array
                    //  key: response
                    //  value: number of answers for this response
                    $votes = array_filter($votes, function($val){ return !is_null($val); });
                    $counts = array_count_values($votes);
                    // empty array
                    $votes = []; 

                    // explode like above
                    foreach($counts as $rep => $count) :
                        $vote = sprintf('{ "rep": "%s", "votes": %d}', $rep, $count);
                        array_push($votes, $vote);
                    endforeach;

                endif;

                // PRINT RESULTS IN JS VAR
                printf("<script>let questionnaire_results = [%s]</script>", join(",", $votes));

                // OTHER ANSWERS (FREE EXPRESSION)
                $alt_query = "SELECT rep_alt FROM ".$wpdb->prefix."questionnaire_results WHERE post_id = ".$post->ID." AND rep_alt IS NOT NULL";
                $alt_result = $wpdb->get_results($alt_query, ARRAY_A);
                $rep_alt = array_column($alt_result, "rep_alt");
                $rep_alt = array_unique($rep_alt, SORT_LOCALE_STRING);
                $rep_alt_counts = array_count_values($rep_alt);

                if(empty( $rep_alt_counts )) :
                    print "<p>".__("Other answers: ", "mige")."<strong>0</strong></p>";
                else :
                    $rep_alt_total = count($rep_alt, COUNT_RECURSIVE);
                    printf("<p>".__("Other answers: ", "mige")."<strong>%d</strong></p>", $rep_alt_total);

                    // BEGIN LIST
                    print "<ol>";
                    
                    $rep_alt_counts = array_flip($rep_alt_counts);

                    foreach($rep_alt_counts as $rep) :
                        printf("<li>%s</li>", stripcslashes($rep));
                    endforeach;

                    // END LIST
                    print "</ol>";
                endif;
                ////---------- END OF QUESTIONNAIRE ON EDIT PAGE ----------////

            endif;
        }
    questionnaire_display_results();

?>
                    </div>
                </li>
            </ul>
        </section>

        <script>
// CREDIT
// adapted from Mario Vidor's pen
// @author Mario Vidor
// @link https://codepen.io/mel/pen/kHCvr
function captchaCode() {
    let nb0, nb1, nb2, nb3, captchaCode;     
    nb0 = (Math.ceil(Math.random() * 10)-1).toString();
    nb1 = (Math.ceil(Math.random() * 10)-1).toString();
    nb2 = (Math.ceil(Math.random() * 10)-1).toString();
    nb3 = (Math.ceil(Math.random() * 10)-1).toString();

    captchaCode = nb0+nb1+nb2+nb3;

    // REPLACE CURRENT CAPTCHA CODE
    let captchaSpan = document.getElementById('captchacode');
    let newSpan = document.createElement("span");
    let newSpanCode = document.createTextNode("<?php __('Enter captcha code: ', 'mige'); ?>" + captchaCode);

    newSpan.setAttribute("id", "captchacode");
    newSpan.appendChild(newSpanCode);

    captchaSpan.parentNode.replaceChild(newSpan, captchaSpan);

    // REMOVE INVALID CLASS FROM INPUT
    let captchaInput = document.getElementById("q__captcha");

    if(captchaInput.classList.contains("invalid")){
        captchaInput.classList.remove("invalid")
    }
}

function validateCaptcha(){
    let captchaCode = document.getElementById("captchacode").innerHTML;
    let enteredCode = document.getElementById("q__captcha").value;
    let submitButton = document.getElementById("q__submit");

    // validated code
    if(enteredCode == captchaCode){
        submitButton.removeAttribute("disabled");
        submitButton.classList.remove("disabled");
    }
    else{
        submitButton.classList.add("invalid");   
    }
}

// EVENT LISTENER ON CAPTCHA RENEW BUTTON
function renewCaptcha(el){
    el.addEventListener = ("click", () => {
        captchaCode();
    });
}
document.getElementById("renewcaptcha").addEventListener("click", () => {
    captchaCode()
});

document.getElementById("q__captcha").addEventListener("blur", () => {
    validateCaptcha();
});
        </script>

<?php
endwhile; // end of loop
?>

  </main>

<?php get_sidebar(); ?>

</div>

<?php
get_footer();

