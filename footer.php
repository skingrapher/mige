<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mige
 */

?>

<footer class="black-text">
  <div class="row">

    <!-- SOCIAL ICONS -->
    <div class="col s8 offset-s2 center-align">

      <a href="https://framasphere.org/i/5974976f65ff" title="diaspora sur framasphere">
				<svg class="social" aria-labelledby="simpleicons-diaspora-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
					<title id="simpleicons-diaspora-icon">Diaspora icon</title>
                    <desc>compte Diaspora</desc>
					<path d="M15.26 21.896l-2.332-3.256c-.622-.87-1.127-1.547-1.154-1.547s-1.006 1.314-2.316 3.113C8.21 21.92 7.178 23.32 7.163 23.32c-.033 0-4.498-3.144-4.51-3.177-.006-.016 1.005-1.498 2.242-3.293 1.24-1.795 2.254-3.29 2.254-3.326 0-.055-.408-.193-3.557-1.245L0 11.08c-.03-.018.156-.64.793-2.65.46-1.446.844-2.64.855-2.655.014-.016 1.71.524 3.772 1.205 2.063.68 3.765 1.234 3.788 1.234.022 0 .046-.03.053-.07.01-.03.03-1.786.04-3.9.02-2.1.04-3.84.05-3.87.02-.03.6-.03 2.73-.03 1.484 0 2.713.015 2.733.03.025.016.065 1.186.136 3.78.11 4.275.11 4.335.18 4.335.025 0 1.66-.54 3.63-1.22 1.973-.66 3.592-1.2 3.605-1.186.03.044 1.65 5.31 1.635 5.325-.017.016-1.667.585-3.66 1.26-2.76.93-3.647 1.245-3.647 1.29-.014.03.93 1.455 2.146 3.21 1.184 1.74 2.143 3.165 2.143 3.18-.015.046-4.44 3.302-4.483 3.302-.015 0-.585-.766-1.245-1.695l.005-.067z"/>
				</svg>
			</a>

	    <a href="https://www.facebook.com/groups/lamige/" title="facebook">
				<svg class="social" aria-labelledby="simpleicons-facebook-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
					<title id="simpleicons-facebook-icon">Facebook icon</title>
                    <desc>compte Facebook</desc>
					<path d="M22.676 0H1.324C.593 0 0 .593 0 1.324v21.352C0 23.408.593 24 1.324 24h11.494v-9.294H9.689v-3.621h3.129V8.41c0-3.099 1.894-4.785 4.659-4.785 1.325 0 2.464.097 2.796.141v3.24h-1.921c-1.5 0-1.792.721-1.792 1.771v2.311h3.584l-.465 3.63H16.56V24h6.115c.733 0 1.325-.592 1.325-1.324V1.324C24 .593 23.408 0 22.676 0"/>
				</svg>
			</a>

      <a href="https://www.youtube.com/channel/UCvxzOW3tHS8Yuc_gBAkK7nA" title="youtube">
				<svg class="social" aria-labelledby="simpleicons-youtube-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
					<title id="simpleicons-youtube-icon">YouTube icon</title>
                    <desc>chaîne YouTube</desc>
					<path class="a" d="M23.495 6.205a3.007 3.007 0 0 0-2.088-2.088c-1.87-.501-9.396-.501-9.396-.501s-7.507-.01-9.396.501A3.007 3.007 0 0 0 .527 6.205a31.247 31.247 0 0 0-.522 5.805 31.247 31.247 0 0 0 .522 5.783 3.007 3.007 0 0 0 2.088 2.088c1.868.502 9.396.502 9.396.502s7.506 0 9.396-.502a3.007 3.007 0 0 0 2.088-2.088 31.247 31.247 0 0 0 .5-5.783 31.247 31.247 0 0 0-.5-5.805zM9.609 15.601V8.408l6.264 3.602z"/>
				</svg>
			</a>

      <a href="https://twitter.com/LaMige23" title="twitter">
				<svg class="social" aria-labelledby="simpleicons-twitter-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
					<title id="simpleicons-twitter-icon">Twitter icon</title>
                    <desc>compte Twitter</desc>
					<path d="M23.954 4.569c-.885.389-1.83.654-2.825.775 1.014-.611 1.794-1.574 2.163-2.723-.951.555-2.005.959-3.127 1.184-.896-.959-2.173-1.559-3.591-1.559-2.717 0-4.92 2.203-4.92 4.917 0 .39.045.765.127 1.124C7.691 8.094 4.066 6.13 1.64 3.161c-.427.722-.666 1.561-.666 2.475 0 1.71.87 3.213 2.188 4.096-.807-.026-1.566-.248-2.228-.616v.061c0 2.385 1.693 4.374 3.946 4.827-.413.111-.849.171-1.296.171-.314 0-.615-.03-.916-.086.631 1.953 2.445 3.377 4.604 3.417-1.68 1.319-3.809 2.105-6.102 2.105-.39 0-.779-.023-1.17-.067 2.189 1.394 4.768 2.209 7.557 2.209 9.054 0 13.999-7.496 13.999-13.986 0-.209 0-.42-.015-.63.961-.689 1.8-1.56 2.46-2.548l-.047-.02z"/>
				</svg>
			</a>

      <a href="https://framapiaf.org/@LaMige23" title="mastodon sur framapiaf">
				<svg class="social" aria-labelledby="simpleicons-mastodon-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
					<title id="simpleicons-mastodon-icon">Mastodon icon</title>
                    <desc>compte Mastodon</desc>
					<path d="M12.012 0C8.833 0 5.785 1.263 3.537 3.511c-4.697 4.667-4.719 12.257-.052 16.954 4.668 4.695 12.257 4.717 16.953.049.016-.016.033-.031.051-.049 4.684-4.682 4.684-12.273 0-16.954C18.24 1.263 15.191 0 12.012 0zm0 6.713h2.566c-.457.322-.629 1.224-.648 1.769v4.993c0 1.064-.855 1.92-1.918 1.92-1.064 0-1.919-.855-1.919-1.92V8.632c-.001-1.064.854-1.919 1.919-1.919zM12 7.96c-.37 0-.672.3-.672.672 0 .37.301.672.672.672s.671-.301.671-.672-.298-.672-.671-.672zm-6.689.19c1.063 0 1.92.854 1.92 1.918v4.942c.021.549.191 1.449.648 1.77H5.311c-1.063 0-1.918-.855-1.918-1.916v-4.795c0-1.066.855-1.919 1.918-1.919zm13.404 0c1.063 0 1.918.854 1.918 1.918v4.795c0 1.064-.855 1.918-1.918 1.918h-2.568c.457-.322.631-1.223.65-1.771v-4.942c0-1.065.853-1.918 1.918-1.918zM5.301 9.398c-.373 0-.672.3-.672.66 0 .359.299.665.66.665.359 0 .665-.3.665-.669 0-.371-.3-.673-.67-.673l.017.017zm13.426 0c-.371 0-.672.3-.672.66 0 .359.299.665.672.665.369 0 .67-.3.67-.669 0-.371-.299-.673-.67-.673v.017zm-6.727.96c-.37 0-.672.3-.672.659s.301.675.672.675.671-.3.671-.675-.298-.674-.671-.674v.015zm-6.701 1.424c-.371 0-.67.3-.67.675 0 .375.299.674.66.674.359 0 .665-.299.665-.674 0-.375-.3-.675-.67-.675h.015zm13.428 0c-.371 0-.672.3-.672.675 0 .375.299.674.672.674.369 0 .67-.299.67-.674-.001-.375-.299-.675-.67-.675zm-6.727.96c-.37 0-.672.299-.672.674s.301.674.672.674.671-.299.671-.674-.298-.674-.671-.674zm-6.701 1.44c-.371 0-.67.299-.67.664 0 .367.299.676.66.676.359 0 .665-.301.665-.676s-.3-.67-.67-.67l.015.006zm13.428 0c-.371 0-.672.299-.672.664 0 .367.299.676.672.676.369 0 .67-.301.67-.676s-.299-.67-.67-.67v.006z"/>
				</svg>
			</a>

    </div>
    <!-- END OF SOCIAL ICONS -->

    <div id="legal" class="col s12 center-align section">

        <small>
          <a href="/mentions-legales"><?php _e("Legal mentions", "mige"); ?></a>&nbsp;&bullet; <span><a href="http://www.pomoloc.fr">Pomoloc</a>, association loi 1901</span>&nbsp;&bullet; <a href="/wp-admin/"><?php _e("Administration", "mige"); ?></a>&nbsp;&bullet; <span>Thème <a href="http://github.com/skrypte/mige" rel="noreferrer noopener" target="_blank">Mige</a> par <a href="https://www.skingrapher.com">skingrapher</a>
        </small>
    </div>

  </div>

</footer>
<?php
get_template_part("parts/mige", "js");

wp_footer(); 
if (is_front_page()) :
    get_template_part( 'parts/map' );
endif;
if (is_page("contact")) :
    get_template_part( 'parts/authorize' );
endif;

//if( wp_script_is("materialize", "enqueued") ) :
?>

<script>
$( document ).ready(() => {
    $(".button-collapse").sideNav({
      menuWidth: 210, // Default is 300
      edge: 'left',
      onOpen: el => {
        document.querySelector(".leaflet-control-container").style.setProperty("display", "none")
      },
      onClose: el => {
        document.querySelector(".leaflet-control-container").style.setProperty("display", "block")
      }
    });
});
</script>

<?php //endif; ?>

</body>
</html>
