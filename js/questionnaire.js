"use strict";
/*
 * REMOVE NODE ON BUTTON CLICK
 * choices : array of choices for response
 * button : button to remvoe node
 */
function removeNodeOnButtonClick(choices, button){
    // event listener on remove button
    button.addEventListener("click", e => {
        e.preventDefault();
        // remove div and its children
        button.parentNode.remove();
        // reorder each response choice
        // Object.keys outputs an array
        Object.keys(choices.children).forEach( k => {
            let i = parseInt(k, 10)+1;
            choices.children[k].firstElementChild.setAttribute("name", "question[reponse_choix_"+i+"]");
        });
    })
}

/*
 * ADD CHOICE FOR RESPONSE
 */
function addRespChoice(){
    let choices = document.getElementById("q__inputs");
    let counter = choices.children.length + 1;
    // add item field
    let choice = document.getElementById("q__rep").value;

    // create new div for new text input
    let div = document.createElement("div");
    // new choice to add
    let input = document.createElement("input");
    // input attributes
    input.setAttribute("type", "text");
    input.setAttribute("inputmode", "text");
    input.setAttribute("name", "question[reponse_choix_"+counter+"]");
    input.setAttribute("value", choice);

    // button to remove field
    let btnRemove = document.createElement("button");
    let span = document.createElement("span");
    span.setAttribute("class", "dashicons dashicons-minus");
    btnRemove.appendChild(span);

    removeNodeOnButtonClick(choices, btnRemove);

    let f = document.createDocumentFragment();

    div.appendChild(input);
    div.appendChild(btnRemove);

    //choices.insertAdjacentElement("afterbegin", div);
    f.appendChild(div);
    choices.appendChild(f);

    let editRemoveText = document.getElementById("q__edit-remove");

    if(editRemoveText == undefined){
        let fields = document.getElementById("q__fields");

        let p = document.createElement("p");
        p.setAttribute("id", "q__edit-remove");
        editRemoveText = document.createTextNode("Éditez ou retirez un champ.");
        p.appendChild(editRemoveText);

        fields.insertBefore(p, choices);
    }
}

/*
 * ADD CHOICE BUTTON
 */
let addBtn = document.getElementById("q__add");

addBtn.addEventListener("click", e => {
    e.preventDefault();
    addRespChoice();
    // empty value of add item field
    document.getElementById("q__rep").setAttribute("value", "");
});

/*
 * REMOVE CHOICE BUTTONS
 */
let removeButtons = document.querySelectorAll("#q__inputs button");

/*
 * if there is already choice for response at questionnaire opening
 */
removeButtons.forEach( b => {
    let choices = document.getElementById("q__inputs");
    removeNodeOnButtonClick(choices, b);
});
