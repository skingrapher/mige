"use strict";
/*
 * =================
 *
 * GRAPHIC ELEMENTS 
 *
 * =================
 *
 * SVG ELEMENT AS CANVAS
 */

let resultsView = document.getElementById("results__view");

let width = 700,
  height = 300,
  radius = Math.min(width, height) / 2;

if(resultsView != null) {
    var d3Container = "#results__view";
    resultsView.style.setProperty("width", width+"px");
    resultsView.style.setProperty("height", height+"px");
    resultsView.style.setProperty("margin-left", "auto");
    resultsView.style.setProperty("margin-right", "auto");
    resultsView.style.setProperty("font-family", "monospace");
}
else{
    var d3Container = "body"
}

let svg = d3
  .select(d3Container)
  .append("svg")
  .attr("width", width)
  .attr("height", height)
  .append("g")
  .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")"); // translate to center of svg

/*
 * SLICES GROUP
 */
svg.append("g").attr("id", "slices");
/*
 * RESPONSES GROUP
 */
svg.append("g").attr("id", "responses");
/*
 * POLYLINES GROUP 
 */
svg.append("g").attr("id", "lines");
/*
 * LEGEND GROUP 
 */
svg.append("g").attr("id", "legend");

/*
 * ARC
 * ANNULAR SECTOR
 */
let arc = d3
  .arc()
  .innerRadius(radius * 0.4)
  .outerRadius(radius * 0.7);

let outerArc = d3
  .arc()
  .innerRadius(radius * 0.8)
  .outerRadius(radius * 0.8);

/*
 * =================
 *
 * COMPUTING DATA
 *
 * =================
 *
 * DATA
 */

let votes = questionnaire_results;

/*
 * PIE
 * COMPUTES ANGLES
 * TO REPRESENT A TABULAR DATASET
 */

// RETURNS VALUE FOR EACH VOTE
let pie = d3.pie().value(d => {
  return d.votes;
})(votes);

// RETURNS VALUE FOR EACH RESPONSE
let response = d => {
  return d.rep;
};

// TOTAL VOTES
function totalResp(votes){
  let arr = Object.values(votes).map(v => {return v.votes});
  let total = arr.reduce((accumulator, currentValue) => {return accumulator + currentValue});
  return total
}

// PERCENTAGE OF VOTES
function percentResp(numb, total){
  let perc = numb * 100 / total;
  return perc
}

/*
 * COLORS
 * ENCODING DATA
 * TO VISUAL REPRESENTATION
 */
let colors = Object.values(votes).map(() => {
  return d3.interpolateRainbow(Math.random().toFixed(7));
});

let allResp = Object.values(votes).map(vote => {
  return vote.rep
});

let color = d3.scaleOrdinal().domain(allResp).range(colors);

/*
 * =================
 *
 * VIEW 
 *
 * =================
 */

// CREATE SLICE VIEW
let slices = svg
  .select("#slices")
  .selectAll(".slice") // assign elements with class: slice (not existing yet)
  .data(pie, response) // data to join to each element
  .enter() // joining data to elements
  .append("path") // create g element
  .attr("fill", d => {return color(d.data.rep)})
  .attr("stroke-width", "2px")
  .attr("class", "slice"); // each g get the class attribute with value: slice

slices
  .transition()
  .duration(1000)
  .attrTween("d", d => {
    this._current = this._current || d;
    let interpolate = d3.interpolate(this._current, d);
    this._current = interpolate(0);
    return t => {
      return arc(interpolate(t));
    };
  });

slices.exit().remove();

function midAngle(d) {
  let angle = d.startAngle + (d.endAngle - d.startAngle) / 2;
  return angle;
}

function percentageRespView(votes, data){
  let p = percentResp(votes, totalResp(data));
  return d3.format(".1d")(p)
}
// RESPONSE LABELS
let resp = svg
  .select("#responses")
  .selectAll("text")
  .data(pie, response)
  .enter()
  .append("text")
	.attr("fill","#1c1c1c")
  .attr("dy", ".35em")
  .style("font-size", ".9em")
  .style("font-weight", "700")
  .text(d => {return d.data.rep});

resp
  .append("tspan")
  .style("font-weight", "400")
  .text(d => {return " - " + percentageRespView(d.data.votes, votes) + "%"})

resp
  .transition()
  .duration(1000)
  .attrTween("transform", d => {
  
    this._current = this._current || d;
    let interpolate = d3.interpolate(this._current, d);
    this._current = interpolate(0);
    return t => {
      let d2 = interpolate(t);
      let pos = outerArc.centroid(d2);
      pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
      return "translate(" + pos + ")";
      
    };
  })
  .attrTween("text-anchor", d => {
    this._current = this._current || d;
    let interpolate = d3.interpolate(this._current, d);
    this._current = interpolate(0);
    return t => {
      let d2 = interpolate(t);
      return midAngle(d2) < Math.PI ? "start" : "end";
    };
  });

resp.exit().remove();

let lines = svg
  .select("#lines")
  .selectAll("polyline")
  .data(pie, response)
  .enter()
  .append("polyline");
  
 lines
  .style("fill", "none")
  .style("stroke", "#1c1c1c")
  .style("stroke-width", "1px")
  .style("opacity", ".5");


lines
  .transition()
  .duration(1000)
  .attrTween("points", d => {
    this._current = this._current || d;
    let interpolate = d3.interpolate(this._current, d);
    this._current = interpolate(0);
    return t => {
      let d2 = interpolate(t);
      let pos = outerArc.centroid(d2);
      pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
      return [arc.centroid(d2), outerArc.centroid(d2), pos];
    };
  });

lines.exit().remove();

let legend = svg
  .select("#lines")
  .selectAll("text")
  .data([totalResp(votes)])
  .enter()
  .append("g")
  .attr("fill","#1c1c1c")
  .attr("transform", "translate(-" + width / 2 + ",-" + height / 2 + ")")
  .append("text")
  .attr("x", width/2)
  .attr("y", height/2);

let legend1 = legend
  .append("tspan")
  .style("font-size", "2.8em")
  .style("font-weight", "700")
  .attr("x", width/2)
  .attr("text-anchor", "middle")
  .attr("alignment-baseline", "middle")
  .text(d => {return d});

let legend2 = legend
  .append("tspan")
  .attr("dy", "1.5em")
  .attr("x", width/2)
  .attr("text-anchor", "middle")
  .attr("alignment-baseline", "middle")
  .text("VOTES")
