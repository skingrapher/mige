<?php
/**
* The sidebar containing the main widget area
* @package mige
*/

/*
 * LAST POSTS
 * COMMON POSTS
 * QUESTIONNAIRES
 */
if(is_single()) :
    $current_id = get_the_ID();
else :
    $current_id = "";
endif;

// l: list of posts
$mige_actu = new WP_Query([
    "post_type"         => "post",
    "post_status"       => "publish",
    "posts_per_page"	=> 3,
    "offset"			=> 0,
    "order"				=> "DESC",
    "orderby"           => "date",
    "post__not_in"      => [$current_id]
]);
?>

<aside class="col s12 m3">

<?php

get_search_form();

if ( $mige_actu->have_posts() ) : ?>

    <h2><?php _e("Last posts", "mige"); ?></h2>

        <ul>
<?php

    while ($mige_actu->have_posts()) : $mige_actu->the_post(); ?>

            <li class="section">
                <a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
            </li>
<?php
        // put a line between each item of the list except the last
        // @link http://materializecss.com/grid.html#grid-layouts 
        if ( $mige_actu->current_post + 1 < $mige_actu->post_count ) :
?>
            <li class="divider" aria-hidden="true"></li>
<?php
        endif; // end of divider display condition
    endwhile;
?>

        </ul>

<?php

wp_reset_postdata();
endif; // end of the secondary loop

$mige_question = new WP_Query([
    "post_type"         => "questionnaire",
    "post_status"       => "publish",
    "posts_per_page"	=> 1,
    "offset"			=> 0,
    "order"				=> "DESC",
    "orderby"           => "date",
    "post__not_in"      => [$current_id]
]);

if ( $mige_question->have_posts() ) : ?>
<h2><?php _e("Questionnaire", "mige"); ?></h2>

        <ul>
<?php

    while ($mige_question->have_posts()) : $mige_question->the_post(); ?>

            <li class="section">
                <a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
            </li>
<?php
    endwhile;
?>

        </ul>

<?php
wp_reset_postdata();
endif; // end of the question loop

/*
 * TAG CLOUD
 */
$all_wp_tags = get_tags([
    "hide_empty"    => true,
    "orderby"       => "count",
    "order"         => "ASC"
]);

// verify if there are tags
if (empty($all_wp_tags) == false) : ?>

    <section class="section">

        <h2><?php _e("Keywords", "mige"); ?></h2>

<?php

    foreach (get_tags() as $tag) {
        $tag_link = get_tag_link( $tag->term_id );
        $tag_markup = "<span class=\"chip\"><a href=\"%s\">%s</a></span>";
        printf($tag_markup, $tag_link, $tag->name);
    }

?>
    </section>
<?php

endif;


get_template_part("parts/markets", "sidebar");
get_template_part("parts/producers", "sidebar");
get_template_part("parts/services", "sidebar");

// add widgets

if (is_active_sidebar("right_sidebar")) : ?>

        <section class="section"><?php dynamic_sidebar("right_sidebar"); ?></section>
<?php
endif;
?>
</aside>
