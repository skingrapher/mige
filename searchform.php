<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
    <div class="input-field">
        <span class="material-icons small prefix" aria-hidden="true">search</span>
        <span class="screen-reader-text"><?php _e("Search", "mige"); ?></span>
        <input id="search__text" type="search" placeholder="un mot" name="s" value="<?php print esc_attr( get_search_query() ); ?>"></input>
        <label for="search__text"><?php _e("Search","mige"); ?></label>
    </div>
</form>
