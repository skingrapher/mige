# MIGE
                           
## WordPress Theme

- WordPress Theme with an Open Street Map on front-page
- Data on markers
- responsive
- accessible
- categories removed
- custom posts: markets, services, producers, questionnaires
- custom taxonomies: places, trades, cases, products
- using transients for requests to database
- using WP cache native functions from WordPress to speed up page display

This theme is not for general purpose. It has specific needs with [Participants Database](https://xnau.com/work/wordpress-plugins/participants-database/) interaction.

### Requirements

- WordPress 4.9.4 minimum (not tested on previous versions)
- [Participants Database](https://xnau.com/work/wordpress-plugins/participants-database/) to get queries from DB and visualize data on map
- an API key from [Open Cage Data](https://geocoder.opencagedata.com/) for geocoding.
 
### License

License: [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0-standalone.html)

A WordPress theme called Mige.

### Version

latest version: 1.0.0

### Installation

This theme is not referenced in official WordPress themes repo.

You have to install by yourself.

#### Manual install

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

Appearance entry in side menu in WP admin interface is removed after installing the theme. You can access to page where you can deactivate the theme at this address of your site : /wp-admin/themes.php.

### Credits
 
Based on:

- [Underscores](https://underscores.me/), © 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
- [Normalize.css](https://necolas.github.io/normalize.css/) v7.0.0, © 2012-2018 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
- [Materialize.css](http://materializecss.com/), v 0.100.2, © 2017-2018 Alvin Wang and Alan Chang, [MIT](https://opensource.org/licenses/MIT)


Semantic Versioning:

- [2.0.0](https://semver.org/)
